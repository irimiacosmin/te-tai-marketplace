pragma solidity ^0.5.10;
import "./usingOraclize.sol";

contract TimeMachine is usingProvable {

    uint256 public timestamp;
    event LogConstructorInitiated(string nextStep);
    event LogPriceUpdated(string price);
    event LogNewProvableQuery(string description);

    constructor () public payable {
		OAR = OracleAddrResolverI(0x6f485C8BF6fc43eA212E93BBF8ce046C7f1cb475);
        emit LogConstructorInitiated("Constructor was initiated. Call 'updateTime()' to send the Provable Query.");
		updateTime();
    }

    function pay() public payable {}

    function balance() public view returns(uint256)  {
        return address(this).balance;
    }

    function __callback(bytes32, string memory result) public {
        if (msg.sender != provable_cbAddress()) revert();
        timestamp = parseInt(result);
        emit LogPriceUpdated(result);
        updateTime();
    }

    function updateTime() public payable {
        if (provable_getPrice("URL") > address(this).balance) {
            emit LogNewProvableQuery("Provable query was NOT sent, please add some ETH to cover for the query fee");
        } else {
            emit LogNewProvableQuery("Provable query was sent, standing by for the answer..");
            provable_query("URL", "json(http://worldtimeapi.org/api/timezone/Europe/Bucharest).unixtime");
        }
    }
}

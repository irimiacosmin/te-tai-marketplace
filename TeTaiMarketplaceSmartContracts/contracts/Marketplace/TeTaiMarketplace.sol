pragma solidity ^0.5.10;

import './TeTaiToken.sol';
import './TimeMachine.sol';

contract TeTaiMarketplace{

    TeTaiToken internal _associatedToken;
	TimeMachine _timeMachine;

    enum ExpertiseType { NONE, KILLER, DEBT_COLLECTOR}

    enum UserType { MANAGER, FREELANCER, ASSESSOR }

    uint public _money;
    address private _contractAddress;
    address private _ownerAddress;

    uint public _managersCount;
    uint public _freelancersCount;
    uint public _assessorsCount;
    uint public _tasksCount;

    struct User {
        bool _exists;
        string _name;
        address _address;
        uint256 _reputation;
        uint256 _tariff;
        uint256 _tokens;
        ExpertiseType _expertiseType;
        UserType _userType;
        mapping(uint => string) _notifications;
    }

    struct Task {
        uint _taskId;
        string _description;

        uint _freelancerReward;
        uint _assessorReward;

        TeTaiMarketplace.ExpertiseType _expertiseType;

        uint256[] timeArray;
		uint8 _timeLen;
        
        // 0 : evaluationTime in seconds
        // 1 : freelanceTime in seconds
        // 2 : starting time
        // 3 : finish time
        // 4 : accept / reject time
        // 5 : evaluation time

        address _managerAddress;
        address _freelancerAddress;
        address _assessorAddress;

        address[] _freelancerArray;

        mapping(address => uint256) _freelancers;
        mapping(address => uint256) _assessors;

        bool isReady;
        bool isDone;

        string _taskTestingDetails;
    }

    mapping(uint => Task) public _tasks;

    mapping(address => User) public _managers;
    mapping(address => User) public _freelancers;
    mapping(address => User) public _assessors;
	
	function getTime() public returns (uint256) {
		_timeMachine.updateTime();
        return _timeMachine.timestamp();
    } 
		
	function payTimeMachine() public payable {
		_timeMachine.pay();
	}
	
	function getBalanceOfTimeMachine() public view returns (uint256) {
        return _timeMachine.balance();
    }
	
	function getTimeFromTask(uint256 taskId, uint256 index) public view returns (uint256) {
        require(taskId <= _tasksCount, "Task does not exist.");
		require(index < 6 && index >= 0);
        return _tasks[taskId].timeArray[index];
    } 
	
    function getFreelancerFromTask(uint256 taskId, address addr) external view returns (bool) {
        require(taskId <= _tasksCount, "Task does not exist.");
        return _tasks[taskId]._freelancers[addr] > 0;
    }

    function getAssessorFromTask(uint256 taskId, address addr) external view returns (bool) {
        require(taskId <= _tasksCount, "Task does not exist.");
        return _tasks[taskId]._assessors[addr] > 0;
    }

    function getNotificationFromUser(address userAddr, uint256 taskId, uint256 userTypeValue)
    external view returns (string memory) {
        require(uint(UserType.ASSESSOR) >= userTypeValue, "Invalid userType provided");
        require(taskId <= _tasksCount, "Task does not exist.");

        UserType userType = UserType(userTypeValue);
        if (userType == UserType.MANAGER) {
            return _managers[userAddr]._notifications[taskId];
        } else if (userType == UserType.FREELANCER) {
            return _freelancers[userAddr]._notifications[taskId];
        } else {
            return _assessors[userAddr]._notifications[taskId];
        }
    }

    modifier onlyManagers {
        require(_managers[msg.sender]._exists, "Method can be called just by Managers");
        _;
    }

    modifier onlyFreelancers {
        require(_freelancers[msg.sender]._exists, "Method can be called just by Freelancers");
        _;
    }

    modifier onlyAssessors {
        require(_assessors[msg.sender]._exists, "Method can be called just by Assessors");
        _;
    }

    modifier userIsNotRegistered {
        require(!_managers[msg.sender]._exists, "This user is already registered as Manager");
        require(!_assessors[msg.sender]._exists, "This user is already registered as Assessor");
        require(!_freelancers[msg.sender]._exists, "This user is already registered as Freelancer");
        _;
    }

    constructor(address _addr) public payable{
        _ownerAddress = msg.sender;
        _money = 100000;
        _associatedToken = new TeTaiToken("TeTaiToken", "&", 3, _money);
        _contractAddress = address(this);
		_timeMachine = TimeMachine(_addr);
		_timeMachine.pay();
		_timeMachine.updateTime();
        _managersCount = 0;
        _freelancersCount = 0;
        _assessorsCount = 0;
        _tasksCount = 0;
    }

    function createManager(string memory name, address payable addr)
    internal pure returns (User memory) {
        return User(true, name, addr, 5, 0, 0, ExpertiseType.NONE, UserType.MANAGER);
    }

    function createFreelancer(string memory name, address payable addr, uint256 tariff, ExpertiseType expertiseType)
    internal pure returns (User memory) {
        return User(true, name, addr, 5, tariff, 0, expertiseType, UserType.FREELANCER);
    }

    function createAssessor(string memory name, address payable addr, uint256 tariff, ExpertiseType expertiseType)
    internal pure returns (User memory) {
        return User(true, name, addr, 5, tariff, 0, expertiseType, UserType.ASSESSOR);
    }

    function createTask(uint taskId, string memory description, uint freelancerReward, uint assessorReward,
        TeTaiMarketplace.ExpertiseType expertiseType, address payable managerAddress) internal pure returns (Task memory) {
        address[] memory temp;
        uint256[] memory timeArray;
        return Task(taskId, description, freelancerReward, assessorReward, expertiseType, timeArray, 0, 
                managerAddress, address(0), address(0), temp, false, false, "");
    }

    function addManager(User memory manager) private returns(bool) {
        require(_managers[manager._address]._exists == false, "Manager already exist.");
        _managers[manager._address] = manager;
        _managersCount++;
        transferTaiTokenFromContract(manager._address, UserType.MANAGER, 1000);
        return true;
    }

    function addFreelancer(User memory freelancer) private returns(bool) {
        require(_freelancers[freelancer._address]._exists == false, "Freelancer already exist.");
        _freelancers[freelancer._address] = freelancer;
        _freelancersCount++;
        transferTaiTokenFromContract(freelancer._address, UserType.FREELANCER, 1000);
        return true;
    }

    function addAssessor(User memory assessor) private returns(bool) {
        require(_assessors[assessor._address]._exists == false, "Assessor already exist.");
        _assessors[assessor._address] = assessor;
        _assessorsCount++;
        transferTaiTokenFromContract(assessor._address, UserType.ASSESSOR, 1000);
        return true;
    }

    function getBalanceOf(address _addr) external view returns(uint256) {
        return _associatedToken.balanceOf(_addr);
    }

    function transferTaiTokenFromContract(address _to, UserType userType, uint amount) internal {
        _money = SafeMath.sub(_money, amount);
        _associatedToken.approve(_contractAddress, amount);
        _associatedToken.transferFrom(_contractAddress, _to, amount);
        if (userType == UserType.MANAGER) {
            _managers[_to]._tokens = SafeMath.add(amount, _managers[_to]._tokens);
        } else if (userType == UserType.FREELANCER) {
            _freelancers[_to]._tokens = SafeMath.add(amount, _freelancers[_to]._tokens);
        } else {
            _assessors[_to]._tokens = SafeMath.add(amount, _assessors[_to]._tokens);
        }
        emit TeTaiSendTokens(amount, _contractAddress, _associatedToken.balanceOf(_contractAddress));
    }

    function transferTaiTokenToContract(address payable _from, UserType userType, uint amount) internal {
        _money = SafeMath.add(_money, amount);
        _associatedToken.approve(_from, amount);
        _associatedToken.transferFrom(_from, _contractAddress, amount);
        if (userType == UserType.MANAGER) {
            _managers[_from]._tokens = SafeMath.sub(_managers[_from]._tokens, amount);
        } else if (userType == UserType.FREELANCER) {
            _freelancers[_from]._tokens = SafeMath.sub(_freelancers[_from]._tokens, amount);
        } else {
            _assessors[_from]._tokens = SafeMath.sub(_assessors[_from]._tokens, amount);
        }
        emit TeTaiReceivedTokens(amount, _from, _associatedToken.balanceOf(_contractAddress));
    }

    function registerFreelancer(string calldata name, uint tariff, uint expertiseTypeValue) external userIsNotRegistered returns (bool) {
        require(uint(ExpertiseType.DEBT_COLLECTOR) >= expertiseTypeValue, "ExpertiseType does not exist.");
        return addFreelancer(createFreelancer(name, msg.sender, tariff, ExpertiseType(expertiseTypeValue)));
    }

    function registerAssesor(string calldata name, uint tariff, uint expertiseTypeValue) external userIsNotRegistered returns (bool) {
        require(uint(ExpertiseType.DEBT_COLLECTOR) >= expertiseTypeValue, "ExpertiseType does not exist.");
        return addAssessor(createAssessor(name, msg.sender, tariff, ExpertiseType(expertiseTypeValue)));
    }

    function registerManager(string calldata name) external userIsNotRegistered returns (bool) {
        return addManager(createManager(name, msg.sender));
    }

    function addTask(string calldata description, uint freelancerReward, uint assessorReward,
        uint expertiseTypeValue) external onlyManagers {
        require(uint(ExpertiseType.DEBT_COLLECTOR) >= expertiseTypeValue, "ExpertiseType does not exist.");

        _tasksCount++;
        _tasks[_tasksCount] = createTask(_tasksCount, description, freelancerReward,
            assessorReward, ExpertiseType(expertiseTypeValue), msg.sender);

        transferTaiTokenToContract(msg.sender, UserType.MANAGER, SafeMath.add(freelancerReward, assessorReward));
    }

    function editTask(uint taskId, string calldata description, uint freelancerReward, uint assessorReward,
        uint expertiseTypeValue) external onlyManagers {
        require(taskId <= _tasksCount, "Task does not exist.");
        require(uint(ExpertiseType.DEBT_COLLECTOR) >= expertiseTypeValue, "ExpertiseType does not exist.");
        require(_tasks[taskId]._managerAddress == msg.sender, "Only the Manager of the task can call this method.");
        require(_tasks[taskId]._freelancerAddress == address(0),
            "You cannot edit this task anymore. A freelancer is already assigned.");
        require(_tasks[taskId]._assessorAddress == address(0),
            "You cannot edit this task anymore. A assessor is already assigned.");

        _tasks[taskId]._description = description;
        _tasks[taskId]._expertiseType = ExpertiseType(expertiseTypeValue);

        uint256 frlRewardDiff;
        uint256 asrRewardDiff;

        if(_tasks[taskId]._freelancerReward > freelancerReward) {
            frlRewardDiff = SafeMath.sub(_tasks[taskId]._freelancerReward, freelancerReward);
            transferTaiTokenFromContract(msg.sender, UserType.MANAGER, frlRewardDiff);
        } else if(_tasks[taskId]._freelancerReward < freelancerReward){
            frlRewardDiff = SafeMath.sub(freelancerReward, _tasks[taskId]._freelancerReward);
            transferTaiTokenToContract(msg.sender, UserType.MANAGER, frlRewardDiff);
        }
        _tasks[taskId]._freelancerReward = freelancerReward;

        if(_tasks[taskId]._assessorReward > assessorReward) {
            asrRewardDiff = SafeMath.sub(_tasks[taskId]._assessorReward, assessorReward);
            transferTaiTokenFromContract(msg.sender, UserType.MANAGER, asrRewardDiff);
        } else if(_tasks[taskId]._assessorReward < assessorReward){
            asrRewardDiff = SafeMath.sub(assessorReward, _tasks[taskId]._assessorReward);
            transferTaiTokenToContract(msg.sender, UserType.MANAGER, asrRewardDiff);
        }
        _tasks[taskId]._assessorReward = assessorReward;
    }

    function acceptToEvaluateTask(uint256 taskId, uint256 timeEstimateHours, uint256 timeEstimateMinutes) onlyAssessors external {
        require(timeEstimateHours >= 0, "Task hours cannot be negative or zero.");
        require(timeEstimateHours < 73, "Tasks can't take longer than 72 hours.");
		require(timeEstimateMinutes >= 0, "Task minutes cannot be negative or zero.");
        require(timeEstimateMinutes < 60, "Tasks can't have more than 60 minutes.");
		require(SafeMath.add(timeEstimateMinutes, timeEstimateHours) > 0, "Tasks must take at least one minute.");
        require(taskId <= _tasksCount, "Task does not exist."); 
        require(_tasks[taskId]._assessorAddress == address(0),
            "You cannot accept to evaluate this task anymore. A assessor is already assigned.");
        require(_tasks[taskId]._freelancerAddress == address(0),
            "You cannot accept to evaluate this task anymore. A freelancer is already assigned.");
        require(_tasks[taskId]._assessors[msg.sender] > 0, "You aren't one of the wanted assessors for this task.");
        require(_tasks[taskId]._expertiseType == _assessors[msg.sender]._expertiseType,
            "You are not a expert in this domain and you cannot accept to evaluate this task.");
			
		uint256 timeValue = SafeMath.add(SafeMath.mul(timeEstimateHours, 3600), SafeMath.mul(timeEstimateMinutes, 60));
        _tasks[taskId]._assessors[msg.sender] = timeValue;
        _tasks[taskId]._assessorAddress = msg.sender;
        _tasks[taskId].timeArray.push(timeValue);
        _tasks[taskId]._timeLen = _tasks[taskId]._timeLen + 1;
    }

    function addAssessorToTask(uint256 taskId, address assessorAddress) external {
        require(taskId <= _tasksCount, "Task does not exist.");
        require(_tasks[taskId]._managerAddress == msg.sender, "Only the Manager of the task can call this method.");
        require(_tasks[taskId]._assessorAddress == address(0),
            "You cannot add assessors to this task anymore. A assessor is already assigned.");
        require(_tasks[taskId]._freelancerAddress == address(0),
            "You cannot add assessors to this task anymore. A freelancer is already assigned.");
        require(_assessors[assessorAddress]._exists, "The address you provided is not assigned to any assessor.");
        require(_tasks[taskId]._assessors[assessorAddress] == 0,
            "The assessor you provided is already introduced in task array.");
        require(_tasks[taskId]._expertiseType == _assessors[assessorAddress]._expertiseType,
            "You are not a expert in this domain and you cannot accept to evaluate this task.");
        _tasks[taskId]._assessors[assessorAddress] = 1;
    }

    function applyToFreelanceTask(uint256 taskId, uint256 timeEstimateHours, uint256 timeEstimateMinutes) onlyFreelancers external {		
        require(timeEstimateHours >= 0, "Task hours cannot be negative or zero.");
        require(timeEstimateHours < 73, "Tasks can't take longer than 72 hours.");
		require(timeEstimateMinutes >= 0, "Task minutes cannot be negative or zero.");
        require(timeEstimateMinutes < 60, "Tasks can't have more than 60 minutes.");
		require(SafeMath.add(timeEstimateMinutes, timeEstimateHours) > 0, "Tasks must take at least one minute.");
		require(taskId <= _tasksCount, "Task does not exist.");		
        require(_tasks[taskId]._assessorAddress != address(0),
            "The task has no assessor assigned to it. Freelances cannot apply yet.");
        require(_tasks[taskId]._freelancerAddress == address(0),
            "Task has a assigned freelancer already. You cannot apply anymore.");
        require(_tasks[taskId]._expertiseType == _freelancers[msg.sender]._expertiseType,
            "You are not a expert in this domain and you cannot accept to evaluate this task.");
		uint256 timeValue = SafeMath.add(SafeMath.mul(timeEstimateHours, 3600), SafeMath.mul(timeEstimateMinutes, 60));

        transferTaiTokenToContract(msg.sender, UserType.FREELANCER, _tasks[taskId]._assessorReward);
        _tasks[taskId]._freelancers[msg.sender] = timeValue;
        _tasks[taskId]._freelancerArray.push(msg.sender);
    }

    function startTask(uint taskId, address freelancerAddress) external {
        require(taskId <= _tasksCount, "Task does not exist.");
        require(_tasks[taskId]._managerAddress == msg.sender, "Only the Manager of the task can call this method.");
        require(_tasks[taskId]._assessorAddress != address(0),
            "The task has no assessor assigned to it. You can't start it yet.");
        require(_tasks[taskId]._freelancerAddress == address(0),
            "Task has a assigned freelancer already. You cannot start it again.");
        require(_freelancers[freelancerAddress]._exists,
            "The address you provided does not match any known freelancer. Please try again.");

        _tasks[taskId]._freelancerAddress = freelancerAddress;
        _tasks[taskId].timeArray.push(_tasks[taskId]._freelancers[freelancerAddress]);
        _tasks[taskId].timeArray.push(getTime());
		_tasks[taskId]._timeLen = _tasks[taskId]._timeLen + 2;

        address[] memory freelancers = _tasks[taskId]._freelancerArray;

        uint rewards = _tasks[taskId]._freelancerReward;
        address frAddr;
        for (uint i=0; i < freelancers.length; i++) {
            frAddr = freelancers[i];
            if (frAddr != freelancerAddress) {
                transferTaiTokenFromContract(frAddr, UserType.FREELANCER, rewards);
            }
        }
    }

    function cancelTask(uint taskId) external {
        require(taskId <= _tasksCount, "Task does not exist.");
        require(_tasks[taskId]._managerAddress == msg.sender, "Only the Manager of the task can call this method.");
        require(_tasks[taskId]._freelancerAddress == address(0),
            "The task has a freelancer assigned to it. You can't cancel it anymore.");
        transferTaiTokenFromContract(msg.sender, UserType.MANAGER, SafeMath.add(_tasks[taskId]._freelancerReward, _tasks[taskId]._assessorReward));
        delete _tasks[taskId];
    }

    function finishTask(uint taskId, string calldata taskTestingDetails) external {
        require(taskId <= _tasksCount, "Task does not exist.");
        require(_tasks[taskId]._freelancerAddress == msg.sender, "Only the Freelancer of the task can call this method.");
		uint256 actualTime = getTime();
		uint256 promisedTime = SafeMath.add(_tasks[taskId].timeArray[1], _tasks[taskId].timeArray[2]);
		
		if (actualTime > promisedTime) {
			uint8 leftOver = _tasks[taskId]._freelancerReward % 2 == 0 ? 0 : 1;
			uint256 cutOff = SafeMath.div(SafeMath.sub(_tasks[taskId]._freelancerReward, leftOver), 2);
			_tasks[taskId]._freelancerReward = cutOff;
			
			Task memory task = _tasks[taskId];
			if(_freelancers[task._freelancerAddress]._reputation > 0) {
                _freelancers[task._freelancerAddress]._reputation = _freelancers[task._freelancerAddress]._reputation - 1;
            }
			
			transferTaiTokenFromContract(task._managerAddress, UserType.MANAGER, SafeMath.add(cutOff, leftOver));
		}
		
        _managers[_tasks[taskId]._managerAddress]._notifications[taskId] = taskTestingDetails;
        _tasks[taskId]._taskTestingDetails = taskTestingDetails;
        _tasks[taskId].isReady = true;
		_tasks[taskId].timeArray.push(actualTime);
		_tasks[taskId]._timeLen = _tasks[taskId]._timeLen + 1;
    }

    function acceptTask(uint taskId) external {
        require(taskId <= _tasksCount, "Task does not exist.");
        Task memory task = _tasks[taskId];
        require(task._managerAddress == msg.sender, "Only the Manager of the task can call this method.");
        require(task.isReady == true, "Task is not ready yet.");
        require(task.isDone == false, "Task is already finished.");
        require(bytes(_managers[msg.sender]._notifications[taskId]).length > 0,
            "You didn't received any instructions on how to test the task result yet.");

        _tasks[taskId].isDone = true;
		_tasks[taskId].timeArray.push(getTime());
		_tasks[taskId]._timeLen = _tasks[taskId]._timeLen + 1;

        if(_freelancers[task._freelancerAddress]._reputation < 10) {
            _freelancers[task._freelancerAddress]._reputation = _freelancers[task._freelancerAddress]._reputation + 1;
        }
        if(_managers[task._managerAddress]._reputation < 10) {
            _managers[task._managerAddress]._reputation = _managers[task._managerAddress]._reputation + 1;
        }

        transferTaiTokenFromContract(task._freelancerAddress, UserType.FREELANCER, SafeMath.add(task._freelancerReward, task._assessorReward));
        transferTaiTokenFromContract(task._managerAddress, UserType.MANAGER, task._assessorReward);
    }

    function rejectTask(uint taskId) external {
        require(taskId <= _tasksCount, "Task does not exist.");
        Task memory task = _tasks[taskId];
        require(task._managerAddress == msg.sender, "Only the Manager of the task can call this method.");
        require(task.isReady == true, "Task is not ready yet.");
        require(task.isDone == false, "Task is already finished.");
        require(bytes(_managers[msg.sender]._notifications[taskId]).length > 0,
            "You didn't received any instructions on how to test the task result yet.");
		_tasks[taskId].timeArray.push(getTime());
		_tasks[taskId]._timeLen = _tasks[taskId]._timeLen + 1;
        _assessors[_tasks[taskId]._assessorAddress]._notifications[taskId] = _tasks[taskId]._taskTestingDetails;
    }
	
	function dissolveTask(uint taskId) external {
        require(taskId <= _tasksCount, "Task does not exist.");
        Task memory task = _tasks[taskId];
        require(task._managerAddress == msg.sender, "Only the Manager of the task can call this method.");
        require(task.isReady == true, "Task is not ready yet.");
        require(task.isDone == false, "Task is already finished.");
		require(task._timeLen == 5, "Task was not rejected yet");
        
		uint256 actualTime = getTime();
		uint256 promisedTime = SafeMath.add(_tasks[taskId].timeArray[0], _tasks[taskId].timeArray[4]);
		
		require(actualTime > promisedTime, "Task evaluation time is not passed yet");
		
		_tasks[taskId].isDone = true;
		_tasks[taskId].timeArray.push(actualTime);
		_tasks[taskId]._timeLen = _tasks[taskId]._timeLen + 1;
		
		if(_assessors[task._assessorAddress]._reputation > 0) {
			_assessors[task._assessorAddress]._reputation = _assessors[task._assessorAddress]._reputation - 1;
        }
		
		_managers[task._managerAddress]._notifications[taskId] = "Task disolved";
		_assessors[task._assessorAddress]._notifications[taskId] = "Task disolved. Your reputation was decremented.";
		
		transferTaiTokenFromContract(task._managerAddress, UserType.MANAGER, SafeMath.add(task._freelancerReward, task._assessorReward));
		transferTaiTokenFromContract(task._freelancerAddress, UserType.FREELANCER, task._assessorReward);
    }

    function arbitrateTask(uint taskId, bool taskIsComplete) external {
        require(taskId <= _tasksCount, "Task does not exist.");
        Task memory task = _tasks[taskId];
        require(task._assessorAddress == msg.sender, "Only the Assessor of the task can call this method.");
        require(task.isReady == true, "Task is not ready yet.");
        require(task.isDone == false, "Task is already finished.");
        require(bytes(_assessors[msg.sender]._notifications[taskId]).length > 0,
            "You didn't received any instructions on how to arbitrate the task yet.");
			
        if (taskIsComplete) {
            transferTaiTokenFromContract(task._freelancerAddress, UserType.FREELANCER, SafeMath.add(task._freelancerReward, task._assessorReward));
            if (_freelancers[task._freelancerAddress]._reputation < 10) {
                _freelancers[task._freelancerAddress]._reputation = _freelancers[task._freelancerAddress]._reputation + 1;
            }
            if(_managers[task._managerAddress]._reputation > 0) {
                _managers[task._managerAddress]._reputation = _managers[task._managerAddress]._reputation - 1;
            }

        } else {
            transferTaiTokenFromContract(task._managerAddress, UserType.MANAGER, SafeMath.add(task._freelancerReward, task._assessorReward));

            if(_freelancers[task._freelancerAddress]._reputation > 0) {
                _freelancers[task._freelancerAddress]._reputation = _freelancers[task._freelancerAddress]._reputation - 1;
            }
        }
        _tasks[taskId].isDone = true;
		uint256 actualTime = getTime();
		uint256 promisedTime = SafeMath.add(_tasks[taskId].timeArray[0], _tasks[taskId].timeArray[4]);
		_tasks[taskId].timeArray.push(actualTime);
		_tasks[taskId]._timeLen = _tasks[taskId]._timeLen + 1;
		
			
		if (actualTime > promisedTime) {
			uint8 leftOver = _tasks[taskId]._assessorReward % 2 == 0 ? 0 : 1;
			uint256 cutOff = SafeMath.div(SafeMath.sub(_tasks[taskId]._assessorReward, leftOver), 2);
			_tasks[taskId]._assessorReward = cutOff;
			
			if(_assessors[task._assessorAddress]._reputation > 0) {
                _assessors[task._assessorAddress]._reputation = _assessors[task._assessorAddress]._reputation - 1;
            }
			
			transferTaiTokenFromContract(task._managerAddress, UserType.MANAGER, SafeMath.add(cutOff, leftOver));
			transferTaiTokenFromContract(task._assessorAddress, UserType.ASSESSOR, _tasks[taskId]._assessorReward);	

		} else {
		
			if (_assessors[task._assessorAddress]._reputation < 10) {
				_assessors[task._assessorAddress]._reputation = _assessors[task._assessorAddress]._reputation + 1;
			}
			transferTaiTokenFromContract(task._assessorAddress, UserType.ASSESSOR, task._assessorReward);	
		}
    }

    event TeTaiReceivedTokens(uint256 amount, address fromAccount, uint256 totalBalance);
    event TeTaiSendTokens(uint256 amount, address fromAccount, uint256 totalBalance);
}
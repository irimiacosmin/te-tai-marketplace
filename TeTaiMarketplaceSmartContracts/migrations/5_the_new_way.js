const MarketPlace = artifacts.require("TeTaiMarketplace");
const TimeMachine = artifacts.require("TimeMachine");

/*
module.exports = (deployer, network, accounts) => {
  const userAddress = accounts[0];
  deployer.deploy(MarketPlace, {from: userAddress, value:10000000000000000000})
  deployer.deploy(TimeMachine, {from: userAddress, value:50000000000000000000})
}
*/

module.exports = (deployer, network, accounts) => {
  const userAddress = accounts[1];
  deployer.deploy(TimeMachine, {from: userAddress, value:40000000000000000000}).then(function() {
    return deployer.deploy(MarketPlace, TimeMachine.address)
  });
};
import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import App from "../App";


class AssessorIsNotRespondingForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonName: props.name,
            disabledValue: props.disabled,
            isManager: true,
            userTypeIndex: 0,
            expertiseTypeIndex: 1,
            open: false,
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };


    handleTaskClose = () => {
        let globalThis = this;
        globalThis.props.onLoading(true);

        let freelancerReward = this.state.freelancerReward;
        let assessorReward = this.state.assessorReward;
        let description = this.state.description;
        let currentUser = this.props.state.users[this.props.state.currentUserIndex];
        let currentTask = this.props.task;
        globalThis.handleClose();

        this.props.state.contract.methods.dissolveTask(currentTask.id)
            .send({from: currentUser.address, gas: 20000000}, function (error, result) {
                if (error === null) {
                    globalThis.props.onSucces(currentUser);
                    globalThis.props.onTaskSucces(globalThis.props.state.contract);
                    globalThis.props.showAlert("Task dissolved successfully.", "success");
                }
                if (result === undefined) {
                    let message = App.getMessageFromError(error);
                    if (message.length < 1) {
                        message = "Error while dissolving the task.";
                    }
                    globalThis.props.showAlert(message, "error");
                }
                globalThis.props.onLoading(false);

            });
    };

    handleChange(event) {
        let val = event.target.value;
        this.setState({userTypeIndex: val, isManager: val === 0});
    }

    render() {
        return (
            <div style={{margin: "10px", marginTop: "15px"}}>
                <Button variant="contained" color="secondary" disabled={this.props.disabled}
                        onClick={this.handleClickOpen}>
                    {this.state.buttonName}
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Disolve task</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            The assessor of the task didn't respond on time.
                            If you disolve the task, all the tokens will go back to their owners but you will lose the result produced by the task.
                        </DialogContentText>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Get me out of here
                        </Button>
                        <Button onClick={this.handleTaskClose} color="secondary">
                            Disolve Task
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default AssessorIsNotRespondingForm;

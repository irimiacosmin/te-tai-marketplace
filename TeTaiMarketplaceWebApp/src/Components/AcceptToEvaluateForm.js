import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import App from "../App";


class AcceptToEvaluateForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonName: props.name,
            disabledValue: props.disabled,
            isManager: true,
            userTypeIndex: 0,
            expertiseTypeIndex: 1,
            tariffValue: null,
            minutesValue: null,
            open: false,
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleTarrifChange = this.handleTarrifChange.bind(this);
        this.handleMinutesChange = this.handleMinutesChange.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleMinutesChange = (e) => {
        this.setState({minutesValue: e.target.value});
    };


    handleAcceptToEvaluate = () => {
        let globalThis = this;
        let freelancerReward = this.state.freelancerReward;
        let assessorReward = this.state.assessorReward;
        let description = this.state.description;
        let currentUser = this.props.state.users[this.props.state.currentUserIndex];
        let currentTask = this.props.task;

        let tariff = this.state.tariffValue;
        let minutesValue = this.state.minutesValue;


        if (tariff === undefined || tariff === null || minutesValue === undefined || minutesValue === null ) {
            globalThis.props.showAlert("Time estimation cannot be empty.", "warning");
            return;
        }
        if (tariff < 0 || tariff > 72) {
            globalThis.props.showAlert("Time estimation hours cannot be lower than 1 hour or bigger than 72 hours.", "warning");
            return;
        }

        if (minutesValue < 0 || minutesValue > 59) {
            globalThis.props.showAlert("Time estimation minutes cannot be lower than 1 hour or bigger than 59 minutes.", "warning");
            return;
        }

        if(Number(tariff) + Number(minutesValue) === 0) {
            globalThis.props.showAlert("Time estimation must be at least one minute.", "warning");
            return;
        }

        globalThis.props.onLoading(true);
        globalThis.handleClose();

        this.props.state.contract.methods.acceptToEvaluateTask(currentTask.id, tariff, minutesValue)
            .send({from: currentUser.address, gas: 20000000}, function (error, result) {
                if (error === null) {
                    globalThis.props.onSucces(currentUser);
                    globalThis.props.onTaskSucces(globalThis.props.state.contract);
                    globalThis.props.showAlert("You applied to evaluate task successfully.", "success");
                }
                if (result === undefined) {
                    let message = App.getMessageFromError(error);
                    if (message.length < 1) {
                        message = "Error while accepting to evaluate task.";
                    }
                    globalThis.props.showAlert(message, "error");
                }
                globalThis.props.onLoading(false);

            });
    };

    handleTarrifChange = (e) => {
        this.setState({tariffValue: e.target.value});
    };

    handleChange(event) {
        let val = event.target.value;
        this.setState({userTypeIndex: val, isManager: val === 0});
    }

    render() {
        return (
            <div style={{margin: "10px", marginTop: "15px"}}>
                <Button variant="outlined" color={this.props.color} disabled={this.props.disabled}
                        onClick={this.handleClickOpen}>
                    {this.state.buttonName}
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Accept to evaluate task</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Accepting to evaluate a task is a operation non-reversible. Are you sure?
                        </DialogContentText>
                        <div style={{marginTop: "5%"}}>
                            <TextField style={{width: "250px", marginRight: "5%"}}
                                       onChange={this.handleTarrifChange}
                                       id="standard-number"
                                       label="Time estimate in hours"
                                       type="number"
                                       InputLabelProps={{
                                           shrink: true,
                                       }}
                            />

                            <TextField style={{width: "250px", marginRight: "5%", marginTop: "20px"}}
                                       onChange={this.handleMinutesChange}
                                       id="standard-number"
                                       label="Time estimate in minutes"
                                       type="number"
                                       InputLabelProps={{
                                           shrink: true,
                                       }}
                            />
                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Get me out of here
                        </Button>
                        <Button onClick={this.handleAcceptToEvaluate} color="secondary">
                            Accept to evaluate task
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default AcceptToEvaluateForm;

import React from "react";
import TaskCard from "./TaskCard";


// const Signature = function (props) {
//     let state = props.state.state;
//     let currentUser = state.users[state.currentUserIndex];
//
//     let task = props.obj.task;
//     let currentTaskAssessor = state.users.find(x => x['address'] === task.assessorAddress);
//
//     let taskStatus = TaskStatus.OPEN;
//     let emptyAddress = "0x0000000000000000000000000000000000000000";
//
//     let backgroundColorStatus = "#e6fcf9";
//     let taskStatusString = "Open";
//     if (task.isDone) {
//         taskStatusString = "Done";
//         taskStatus = TaskStatus.DONE;
//         backgroundColorStatus = "#64f57a";
//     } else if (task.isReady) {
//         taskStatusString = "Ready";
//         taskStatus = TaskStatus.READY;
//         backgroundColorStatus = "#64eef5";
//     } else if (task.freelancerAddress && task.freelancerAddress !== emptyAddress
//         && task.assessorAddress && task.assessorAddress !== emptyAddress) {
//         taskStatusString = "In Progress";
//         taskStatus = TaskStatus.IN_PROGRESS;
//         backgroundColorStatus = "#b0ff9e";
//     }
//
//     let idDiv =
//         <div className="row" id="detailsBoxLeft">
//             <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Task Id:</p>
//             <p style={{fontSize: "16px"}}>{task.id}</p>
//         </div>;
//
//     let statusDiv =
//         <div className="row" id="detailsBoxLeft">
//             <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Task Status:</p>
//             <p style={{fontSize: "16px"}}>{taskStatusString}</p>
//         </div>;
//
//     let descriptionDiv = <div className="row" id="detailsBoxMiddle">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Description:</p>
//         <p style={{fontSize: "16px"}}>{task.description}</p>
//     </div>;
//
//     let expertiseTypeDiv = <div className="row" id="detailsBoxLeft">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Expertise Type:</p>
//         <p style={{fontSize: "16px"}}>{task.expertiseType}</p>
//     </div>;
//
//     let freelancerRewardDiv = <div className="row" id="detailsBoxLeft">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Freelancer Reward:</p>
//         <p style={{fontSize: "16px"}}>{task.freelancerReward}</p>
//     </div>;
//
//     let assessorRewardDiv = <div className="row" id="detailsBoxLeft">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Assessor Reward:</p>
//         <p style={{fontSize: "16px"}}>{task.assessorReward}</p>
//     </div>;
//
//     let managerDiv = <div className="row" id="detailsBoxMiddle">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Manager Address:</p>
//         <p style={{fontSize: "16px"}}>{task.managerAddress != null ? task.managerAddress : "TBA"}</p>
//     </div>;
//
//     let freelancerDiv = <div className="row" id="detailsBoxMiddle">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Freelancer Address:</p>
//         <p style={{fontSize: "16px"}}>{task.freelancerAddress != null ? task.freelancerAddress : "TBA"}</p>
//     </div>;
//
//     let assessorDiv = <div className="row" id="detailsBoxMiddle">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Assessor Address:</p>
//         <p style={{fontSize: "16px"}}>{task.assessorAddress != null ? task.assessorAddress : "TBA"}</p>
//     </div>;
//
//     let allFRDivs = [];
//     for (const [index, value] of task.freelancers.entries()) {
//         allFRDivs.push(
//             <p style={{fontSize: "12px"}}>{value}</p>
//         );
//     }
//
//     let allASDivs = [];
//     for (const [index, value] of task.assessors.entries()) {
//         allASDivs.push(
//             <p style={{fontSize: "12px"}}>{value}</p>
//         );
//     }
//
//     let allFreelancersDiv = <div className="row" id="detailsBoxMiddle">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Freelancers that applied: [ </p>
//         <div>{allFRDivs}</div>
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>]</p>
//     </div>;
//
//
//     let allAssessorsDiv = <div className="row" id="detailsBoxMiddle">
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Assessors wanted: [</p>
//         <div>{allASDivs}</div>
//         <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>]</p>
//     </div>;
//
//     let taskTestingDetails = null;
//     let acceptTaskButton = null, editTask = null, cancelTask = null, finishTask = null, rejectTask = null, startTask = null,
//         addAssessorToTask = null, applyToFreelanceTask = null, acceptToEvaluateTask = null, arbitrateTask = null;
//
//     let currentUserType = currentUser === undefined ? undefined : currentUser.type;
//     switch (taskStatus) {
//         case "Open": {
//             switch (currentUserType) {
//                 case "Manager": {
//                     if(task.managerAddress === currentUser.address) {
//                         if(task.freelancers.length > 0 && task.assessors.length > 0) {
//                             startTask = <StartTaskForm name="Start Task" state={state} task={task}
//                                                        onTaskSucces={props.onTaskSucces}
//                                                        onSucces={props.onSucces}
//                                                        showAlert={props.showAlert}
//                                                        disabled={false} color='primary'>
//                             </StartTaskForm>;
//                         }
//                         editTask = <EditTaskForm name="Edit Task" state={state} task={task}
//                                                  onTaskSucces={props.onTaskSucces}
//                                                  onSucces={props.onSucces}
//                                                  showAlert={props.showAlert}
//                                                  disabled={false} color='primary'>
//                         </EditTaskForm>;
//
//                         addAssessorToTask = <AddAssessorForm name="Add assessor" state={state} task={task}
//                                                              onTaskSucces={props.onTaskSucces}
//                                                              onSucces={props.onSucces}
//                                                              showAlert={props.showAlert}
//                                                              disabled={false} color='default'>
//                         </AddAssessorForm>;
//
//                         cancelTask = <CancelTaskForm name="Cancel Task" state={state} task={task}
//                                                       onTaskSucces={props.onTaskSucces}
//                                                       onSucces={props.onSucces}
//                                                       showAlert={props.showAlert}
//                                                       disabled={false} color='secondary'>
//                         </CancelTaskForm>;
//                     }
//                     break;
//                 }
//                 case "Freelancer": {
//                     let taskHasAssessor = task.assessorAddress !== undefined
//                         && task.assessorAddress !== null
//                         && task.assessorAddress !== "0x0000000000000000000000000000000000000000";
//                     let imIn = taskHasAssessor && task.freelancers.indexOf(currentUser.address) === -1;
//
//                     if (imIn) {
//                         let applyButtonType = task.freelancerAddress === currentUser.address ? 'disabled' : 'primary';
//                         applyToFreelanceTask = <ApplyToWorkOnTaskForm name="Apply to work" state={state} task={task}
//                                                                onTaskSucces={props.onTaskSucces}
//                                                                onSucces={props.onSucces}
//                                                                showAlert={props.showAlert}
//                                                                disabled={false} color={applyButtonType}>
//                         </ApplyToWorkOnTaskForm>;
//                     }
//                     break;
//                 }
//                 case "Assessor": {
//                     let imIn = task.assessorAddress !== currentUser.address && task.assessors.indexOf(currentUser.address) !== -1;
//                     if (imIn) {
//                         let applyButtonType = task.assessorAddress === currentUser.address ? 'disabled' : 'primary';
//                         acceptToEvaluateTask = <AcceptToEvaluateForm name="Accept to evaluate" state={state} task={task}
//                                                                       onTaskSucces={props.onTaskSucces}
//                                                                       onSucces={props.onSucces}
//                                                                       showAlert={props.showAlert}
//                                                                       disabled={false} color={applyButtonType}>
//                         </AcceptToEvaluateForm>;
//                     }
//                     break;
//                 }
//             }
//             break;
//         }
//         case "In progress": {
//             if (currentUserType === "Freelancer" && task.freelancerAddress === currentUser.address) {
//                 finishTask = <FinishTaskForm name="Finish task" state={state} task={task}
//                                              onTaskSucces={props.onTaskSucces}
//                                              onSucces={props.onSucces}
//                                              showAlert={props.showAlert}
//                                              disabled={false} color='primary'>
//                 </FinishTaskForm>;
//             }
//             break;
//         }
//         case "Ready": {
//             switch (currentUserType) {
//                 case "Manager": {
//                     if(task.managerAddress === currentUser.address &&
//                         currentUser.notifications[task.id] && currentTaskAssessor.notifications[task.id] === undefined) {
//                         acceptTaskButton = <AcceptTaskForm name="Accept Task" state={state} task={task}
//                                         onTaskSucces={props.onTaskSucces}
//                                         onSucces={props.onSucces}
//                                         showAlert={props.showAlert}
//                                         disabled={false} color='primary'>
//                         </AcceptTaskForm>;
//                         rejectTask = <RejectTaskForm name="Reject Task" state={state} task={task}
//                                                      onTaskSucces={props.onTaskSucces}
//                                                      onSucces={props.onSucces}
//                                                      showAlert={props.showAlert}
//                                                      disabled={false} color='secondary'>
//                         </RejectTaskForm>;
//                         taskTestingDetails = <div className="row" id="detailsBoxMiddle">
//                             <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Task testing details:</p>
//                             <p style={{fontSize: "16px"}}>{currentUser.notifications[task.id]}</p>
//                         </div>;
//                     }
//                     break;
//                 }
//                 case "Assessor": {
//                     if(task.assessorAddress === currentUser.address &&
//                         currentUser.notifications[task.id] !== undefined) {
//                         arbitrateTask = <ArbitrateTaskForm name="Arbitrate task" task={task} state={state}
//                                                            onTaskSucces={props.onTaskSucces}
//                                                            onSucces={props.onSucces}
//                                                            showAlert={props.showAlert}
//                                                         disabled={false} color='primary'>
//                         </ArbitrateTaskForm>;
//                         taskTestingDetails = <div className="row" id="detailsBoxMiddle">
//                             <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Task testing details:</p>
//                             <p style={{fontSize: "16px"}}>{currentUser.notifications[task.id]}</p>
//                         </div>;
//                     }
//                     break;
//                 }
//             }
//             break;
//         }
//         case "Done": {
//             break;
//         }
//     }
//
//     let finalDiv = null;
//
//     if(task.id > 0) {
//         finalDiv = <div id="signature" className="row" style={{background: backgroundColorStatus}}>
//             <div className="signColumnStatus">
//                 {idDiv}
//                 {expertiseTypeDiv}
//                 {statusDiv}
//                 {freelancerRewardDiv}
//                 {assessorRewardDiv}
//             </div>
//             <div className="signColumnMiddle">
//                 {descriptionDiv}
//                 {managerDiv}
//                 {freelancerDiv}
//                 {assessorDiv}
//                 {allFreelancersDiv}
//                 {allAssessorsDiv}
//                 {taskTestingDetails}
//             </div>
//             <div className="signColumnButtons">
//                 <div className="row">
//                     {acceptTaskButton}
//                     {acceptToEvaluateTask}
//                     {addAssessorToTask}
//                     {applyToFreelanceTask}
//                     {arbitrateTask}
//                     {editTask}
//                     {finishTask}
//                     {rejectTask}
//                     {startTask}
//                     {cancelTask}
//                 </div>
//             </div>
//         </div>
//     }
//
//     return (
//         <div>
//             {finalDiv}
//         </div>
//     )
// };

export default function ResponseBox(props) {
    let state = props.state;
    let k = 1;
    return (

        <div id="signaturesBigBox">
            {props.tasks.map((task, index) => <TaskCard
                key={(k++) * 123} state={{state}}
                onSucces={props.onSucces}
                onLoading={props.onLoading}
                onTaskSucces={props.onTaskSucces}
                refreshTasks={props.refreshTasks}
                showAlert={props.showAlert}
                obj={{task}}
            />)}
        </div>
    )
};

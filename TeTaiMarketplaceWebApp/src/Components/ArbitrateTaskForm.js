import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import App from "../App";

const UserType = {
    MANAGER: 'Manager',
    FREELANCER: 'Freelancer',
    ASSESSOR: 'Assessor'
};

const ExpertiseType = {
    NONE: 'None',
    KILLER: 'Killer',
    DEBT_COLLECTOR: 'Debt collector',
};

const TaskStatus = {
    OPEN: 'Open',
    IN_PROGRESS: 'In progress',
    READY: 'Ready',
    DONE: 'Done'
};

class ArbitrateTaskForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonName: props.name,
            disabledValue: props.disabled,
            isManager: true,
            userTypeIndex: 0,
            expertiseTypeIndex: 1,
            open: false
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleExpertiseChange = this.handleExpertiseChange.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleArbitrateTask = () => {
        let globalThis = this;
        globalThis.props.onLoading(true);

        let freelancerReward = this.state.freelancerReward;
        let assessorReward = this.state.assessorReward;
        let description = this.state.description;
        let currentUser = this.props.state.users[this.props.state.currentUserIndex];
        let currentTask = this.props.task;
        globalThis.handleClose();

        this.props.state.contract.methods.arbitrateTask(currentTask.id, this.state.userTypeIndex === 0)
            .send({from: currentUser.address, gas: 20000000}, function (error, result) {
                if (error === null) {
                    globalThis.props.onSucces(currentUser);
                    globalThis.props.onTaskSucces(globalThis.props.state.contract);
                    globalThis.props.showAlert("Task arbitrated successfully.", "success");
                }
                if (result === undefined) {
                    let message = App.getMessageFromError(error);
                    if (message.length < 1) {
                        message = "Error while arbitrating task.";
                    }
                    globalThis.props.showAlert(message, "error");
                }
                globalThis.props.onLoading(false);

            });
    };

    handleChange(event) {
        let val = event.target.value;
        this.setState({userTypeIndex: val, isManager: val === 0});
    }

    handleExpertiseChange(event) {
        this.setState({expertiseTypeIndex: event.target.value});
    }

    render() {

        return (
            <div style={{margin: "10px", marginTop: "15px"}}>
                <Button variant="outlined" color="primary" disabled={this.props.disabled}
                        onClick={this.handleClickOpen}>
                    {this.state.buttonName}
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Arbitrate task</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please tell us if the task was implemented like the requirements were stated.
                        </DialogContentText>
                        <div style={{marginTop: "5%"}}>
                            <FormControl style={{marginRight: "5%"}}>
                                <InputLabel id="demo-simple-select-label"></InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.userTypeIndex}
                                    onChange={this.handleChange}
                                >
                                    <MenuItem value={0}>Task is complete</MenuItem>
                                    <MenuItem value={1}>Task is not complete</MenuItem>
                                </Select>
                            </FormControl>

                        </div>

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleArbitrateTask} color="primary">
                            Arbitrate Task
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default ArbitrateTaskForm;

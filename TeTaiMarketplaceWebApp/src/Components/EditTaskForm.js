import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import App from "../App";

const UserType = {
    MANAGER: 'Manager',
    FREELANCER: 'Freelancer',
    ASSESSOR: 'Assessor'
};

const ExpertiseType = {
    NONE: 'None',
    KILLER: 'Killer',
    DEBT_COLLECTOR: 'Debt collector',
};

const ExpertiseTypes = ['None', 'Killer', 'Debt collector'];

const TaskStatus = {
    OPEN: 'Open',
    IN_PROGRESS: 'In progress',
    READY: 'Ready',
    DONE: 'Done'
};

class EditTaskForm extends Component {
    constructor(props) {
        super(props);
        let task = this.props.task;

        this.state = {
            buttonName: props.name,
            disabledValue: props.disabled,
            isManager: false,
            userTypeIndex: 0,
            expertiseTypeIndex: ExpertiseTypes.indexOf(task.expertiseType),
            open: false,
            freelancerReward: task.freelancerReward,
            assessorReward: task.assessorReward,
            description: task.description,
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleExpertiseChange = this.handleExpertiseChange.bind(this);
        this.handleFreelancerRewardChange = this.handleFreelancerRewardChange.bind(this);
        this.handleAssessorRewardChange = this.handleAssessorRewardChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleSubmit = () => {
        let globalThis = this;
        let freelancerReward = this.state.freelancerReward;
        let assessorReward = this.state.assessorReward;
        let description = this.state.description;
        let currentUser = this.props.state.users[this.props.state.currentUserIndex];
        let task = this.props.task;

        if (freelancerReward < 1 || assessorReward < 1) {
            globalThis.props.showAlert("Rewards must be greater than 0", "warning");
            return;
        }

        if ((Number(freelancerReward) + Number(assessorReward)) > currentUser.tokens) {
            globalThis.props.showAlert("You don't have enough tokens to create this task.", "warning");
            return;
        }

        if (description === null || description.length < 3) {
            globalThis.props.showAlert("Description must have more than 3 letters", "warning");
            return;
        }
        globalThis.props.onLoading(true);
        globalThis.handleClose();

        this.props.state.contract.methods.editTask(task.id, description, freelancerReward, assessorReward, this.state.expertiseTypeIndex)
            .send({from: currentUser.address, gas: 20000000}, function (error, result) {

                if (error === null) {
                    globalThis.props.showAlert("Task edited successfully", "success");
                    globalThis.props.onTaskSucces(globalThis.props.state.contract);
                    globalThis.props.onSucces(currentUser);
                }
                if (result === undefined) {
                    let message = App.getMessageFromError(error);
                    if (message.length < 1) {
                        message = "Error when editing task";
                    }
                    globalThis.props.showAlert(message, "error");
                }
                globalThis.props.onLoading(false);

            });

    };

    handleChange(event) {
        let val = event.target.value;
        this.setState({userTypeIndex: val, isManager: val === 0});
    }

    handleExpertiseChange(event) {
        this.setState({expertiseTypeIndex: event.target.value});
    }

    handleFreelancerRewardChange(event) {
        this.setState({freelancerReward: event.target.value});
    }

    handleAssessorRewardChange(event) {
        this.setState({assessorReward: event.target.value});
    }

    handleDescriptionChange(event) {
        this.setState({description: event.target.value});
    }

    render() {
        return (
            <div style={{margin: "10px", marginTop: "15px"}}>
                <Button variant="outlined" color="primary" disabled={this.props.disabled}
                        onClick={this.handleClickOpen}>
                    {this.state.buttonName}
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Edit task</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please change the current information about the task .
                        </DialogContentText>
                        <div style={{marginTop: "5%"}}>
                            <FormControl style={{width: "150px", marginRight: "5%"}}
                                         disabled={this.state.isManager}
                            >
                                <InputLabel id="demo-simple-select-label" style={{width: "150px"}}>
                                    Expertise Type</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.expertiseTypeIndex}
                                    onChange={this.handleExpertiseChange}
                                    style={{width: "150px"}}
                                >
                                    <MenuItem value={1}>Killer</MenuItem>
                                    <MenuItem value={2}>Debt collector</MenuItem>
                                </Select>
                            </FormControl>
                            <div style={{marginTop: "15px"}}>
                                <TextField style={{width: "150px", marginRight: "5%"}}
                                           disabled={this.state.isManager}
                                           id="standard-number"
                                           label="Freelancer Reward"
                                           type="number"
                                           value={this.state.freelancerReward}
                                           onChange={this.handleFreelancerRewardChange}
                                           InputLabelProps={{
                                               shrink: true,
                                           }}
                                />

                                <TextField style={{width: "150px", marginRight: "5%"}}
                                           disabled={this.state.isManager}
                                           id="standard-number"
                                           label="Assessor Reward"
                                           type="number"
                                           value={this.state.assessorReward}
                                           onChange={this.handleAssessorRewardChange}
                                           InputLabelProps={{
                                               shrink: true,
                                           }}
                                />
                            </div>
                        </div>

                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Description"
                            value={this.state.description}
                            onChange={this.handleDescriptionChange}
                            type="email"
                            fullWidth
                        />

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleSubmit} color="primary">
                            Edit Task
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default EditTaskForm;

import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import App from "../App";

class FinishTaskForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonName: props.name,
            disabledValue: props.disabled,
            isManager: true,
            userTypeIndex: 0,
            expertiseTypeIndex: 1,
            open: false,
            details: null
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleExpertiseChange = this.handleExpertiseChange.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleDetailsChange = (event) => {
        this.setState({details: event.target.value});
    };

    handleFinishTask = () => {
        let globalThis = this;
        let currentUser = this.props.state.users[this.props.state.currentUserIndex];
        let task = this.props.task;

        if (this.state.details === undefined || this.state.details === null) {
            globalThis.props.showAlert("Details cannot be null.", "warning");
            return;
        }
        if (this.state.length < 10) {
            globalThis.props.showAlert("Details cannot be lower than 10 characters", "warning");
            return;
        }
        globalThis.props.onLoading(true);
        globalThis.handleClose();

        this.props.state.contract.methods.finishTask(task.id, this.state.details)
            .send({from: currentUser.address, gas: 20000000}, function (error, result) {
                if (error === null) {
                    globalThis.props.showAlert("Task completed successfully", "success");
                    globalThis.props.onTaskSucces(globalThis.props.state.contract);
                    globalThis.props.onSucces(currentUser);
                }
                if (result === undefined) {
                    let message = App.getMessageFromError(error);
                    if (message.length < 1) {
                        message = "Error when completing task";
                    }
                    globalThis.props.showAlert(message, "error");
                }
                globalThis.props.onLoading(false);

            });
    };


    handleChange(event) {
        let val = event.target.value;
        this.setState({userTypeIndex: val, isManager: val === 0});
    }

    handleExpertiseChange(event) {
        this.setState({expertiseTypeIndex: event.target.value});
    }

    render() {

        return (
            <div style={{margin: "10px", marginTop: "15px"}}>
                <Button variant="outlined" color="primary" disabled={this.props.disabled}
                        onClick={this.handleClickOpen}>
                    {this.state.buttonName}
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Finish task</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please insert bellow the task testing details
                        </DialogContentText>

                        <TextField
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Task Testing Details"
                            type="email"
                            fullWidth
                            onChange={this.handleDetailsChange}
                        />

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleFinishTask} color="primary">
                            Finish Task
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default FinishTaskForm;

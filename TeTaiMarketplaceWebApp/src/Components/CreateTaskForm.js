import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import SnackbarComponent from "./SnackbarComponent";
import App from "../App";

const UserType = {
    MANAGER: 'Manager',
    FREELANCER: 'Freelancer',
    ASSESSOR: 'Assessor'
};

const ExpertiseType = {
    NONE: 'None',
    KILLER: 'Killer',
    DEBT_COLLECTOR: 'Debt collector',
};

const TaskStatus = {
    OPEN: 'Open',
    IN_PROGRESS: 'In progress',
    READY: 'Ready',
    DONE: 'Done'
};

class CreateTaskForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonName: props.name,
            disabledValue: props.disabled,
            isManager: false,
            userTypeIndex: 0,
            expertiseTypeIndex: 1,
            freelancerReward: null,
            assessorReward: null,
            description: null,
            open: false,
            showMessage: false,
            messageText: null,
            messageSeverity: null
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleExpertiseChange = this.handleExpertiseChange.bind(this);
        this.handleFreelancerRewardChange = this.handleFreelancerRewardChange.bind(this);
        this.handleAssessorRewardChange = this.handleAssessorRewardChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.createTask = this.createTask.bind(this);
        this.showAlert = this.showAlert.bind(this);
        this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
    }

    showAlert(text, severity) {
        this.setState({
            showMessage: true,
            messageText: text,
            messageSeverity: severity
        });
    }

    handleSnackbarClose() {
        this.setState({showMessage: false});
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    createTask = () => {
        let globalThis = this;
        let freelancerReward = this.state.freelancerReward;
        let assessorReward = this.state.assessorReward;
        let description = this.state.description;
        let currentUser = this.props.state.users[this.props.state.currentUserIndex];

        if (freelancerReward < 1 || assessorReward < 1) {
            globalThis.showAlert("Rewards must be greater than 0", "warning");
            return;
        }

        if ((Number(freelancerReward) + Number(assessorReward)) > currentUser.tokens) {
            globalThis.showAlert("You don't have enough tokens to create this task.", "warning");
            return;
        }

        if (description === null || description.length < 3) {
            globalThis.showAlert("Description must have more than 3 letters", "warning");
            return;
        }

        globalThis.props.onLoading(true);
        globalThis.handleClose();


        this.props.state.contract.methods.addTask(description, freelancerReward, assessorReward, this.state.expertiseTypeIndex)
            .send({from: currentUser.address, gas: 20000000}, function (error, result) {
                if (error === null) {
                    globalThis.props.onTaskSucces(globalThis.props.state.contract);
                    globalThis.props.onSucces(currentUser);
                    globalThis.showAlert("Task added successfully", "success");
                }
                if (result === undefined) {
                    let message = App.getMessageFromError(error);
                    if (message.length < 1) {
                        message = "Error when adding task";
                    }
                    globalThis.props.showAlert(message, "error");
                }
                globalThis.props.onLoading(false);

            });
    };

    handleChange(event) {
        let val = event.target.value;
        this.setState({userTypeIndex: val, isManager: val === 0});
    }

    handleExpertiseChange(event) {
        this.setState({expertiseTypeIndex: event.target.value});
    }

    handleFreelancerRewardChange(event) {
        this.setState({freelancerReward: event.target.value});
    }

    handleAssessorRewardChange(event) {
        this.setState({assessorReward: event.target.value});
    }

    handleDescriptionChange(event) {
        this.setState({description: event.target.value});
    }

    render() {
        let message = null;
        if (this.state.showMessage) {
            message = <SnackbarComponent text={this.state.messageText} showMessage={this.state.showMessage}
                                         handleClose={this.handleSnackbarClose}
                                         severity={this.state.messageSeverity}/>
        }

        return (
            <div style={{marginRight: "10px"}}>
                {message}
                <Button variant="contained" disabled={this.props.disabled}
                        onClick={this.handleClickOpen}>
                    {this.state.buttonName}
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title"
                >
                    <DialogTitle id="form-dialog-title">Create task</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please fill in the information needed to create the task.
                        </DialogContentText>
                        <div style={{marginTop: "5%"}}>
                            <FormControl style={{width: "150px", marginRight: "5%"}}
                                         disabled={this.state.isManager}
                            >
                                <InputLabel id="demo-simple-select-label" style={{width: "150px"}}>
                                    Expertise Type</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.expertiseTypeIndex}
                                    onChange={this.handleExpertiseChange}
                                    style={{width: "150px"}}
                                >
                                    <MenuItem value={1}>Killer</MenuItem>
                                    <MenuItem value={2}>Debt collector</MenuItem>
                                </Select>
                            </FormControl>
                            <div style={{marginTop: "15px"}}>
                                <TextField style={{width: "150px", marginRight: "5%"}}
                                           onChange={this.handleFreelancerRewardChange}
                                           disabled={this.state.isManager}
                                           id="standard-number"
                                           label="Freelancer Reward"
                                           type="number"
                                           InputLabelProps={{
                                               shrink: true,
                                           }}
                                />

                                <TextField style={{width: "150px", marginRight: "5%"}}
                                           onChange={this.handleAssessorRewardChange}
                                           disabled={this.state.isManager}
                                           id="standard-number"
                                           label="Assessor Reward"
                                           type="number"
                                           InputLabelProps={{
                                               shrink: true,
                                           }}
                                />
                            </div>
                        </div>

                        <TextField
                            onChange={this.handleDescriptionChange}
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Description"
                            type="email"
                            fullWidth
                        />

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.createTask} color="primary">
                            Create Task
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default CreateTaskForm;

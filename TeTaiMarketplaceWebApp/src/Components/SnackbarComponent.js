import React, {Component} from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import {makeStyles} from '@material-ui/core/styles';

const useStyles = makeStyles(theme => ({
    root: {
        width: '100%',
        '& > * + *': {
            marginTop: theme.spacing(2),
        },
    },
}));

function Alert(props) {
    return <MuiAlert elevation={6} variant="filled" {...props} />;
}

class SnackbarComponent extends Component {
    constructor(props) {
        super(props);
    }

    render() {
        return (
            <div>
                <Snackbar open={this.props.showMessage} autoHideDuration={6000} onClose={this.props.handleClose}>
                    <Alert onClose={this.handleClose} severity={this.props.severity}>
                        {this.props.text}
                    </Alert>
                </Snackbar>
            </div>
        )
    }
}

export default SnackbarComponent;

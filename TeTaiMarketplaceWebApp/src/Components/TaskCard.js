import React, {Component} from 'react';
import {makeStyles} from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import CardActionArea from "@material-ui/core/CardActionArea";
import StartTaskForm from "./StartTaskForm";
import EditTaskForm from "./EditTaskForm";
import AddAssessorForm from "./AddAssessorForm";
import CancelTaskForm from "./CancelTaskForm";
import ApplyToWorkOnTaskForm from "./ApplyToWorkOnTaskForm";
import AcceptToEvaluateForm from "./AcceptToEvaluateForm";
import FinishTaskForm from "./FinishTaskForm";
import AcceptTaskForm from "./AcceptTaskForm";
import RejectTaskForm from "./RejectTaskForm";
import ArbitrateTaskForm from "./ArbitrateTaskForm";
import Chip from "@material-ui/core/Chip";
import ExpansionPanel from "@material-ui/core/ExpansionPanel";
import ExpansionPanelSummary from "@material-ui/core/ExpansionPanelSummary";
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import ExpansionPanelDetails from "@material-ui/core/ExpansionPanelDetails";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemAvatar from "@material-ui/core/ListItemAvatar";
import ListItemText from "@material-ui/core/ListItemText";
import {deepOrange, deepPurple} from "@material-ui/core/colors";
import Countdown from "./Countdown";
import AssessorIsNotRespondingForm from "./AssessorIsNotRespondingForm";


const TaskStatus = {
    OPEN: 'Open',
    IN_PROGRESS: 'In progress',
    READY: 'Ready',
    DONE: 'Done'
};

const genericUser = {
    name: 'To be announced',
    address: '0x0...0'
};

class TaskCard extends Component {

    constructor(props) {
        super(props);
        this.state = props.state;
        this.getSecondsAsString = this.getSecondsAsString.bind(this);
    }

    getSecondsAsString(seconds) {
        let days = Math.floor(seconds / (3600 * 24));
        seconds -= days * 3600 * 24;
        let hrs = Math.floor(seconds / 3600);
        seconds -= hrs * 3600;
        let mnts = Math.floor(seconds / 60);
        seconds -= mnts * 60;
        return days + "d " + hrs + "h " + mnts + "m " + seconds + "s";
    }

    render() {
        const classes = makeStyles(theme => ({
            card: {
                display: 'flex',
                height: '100%'
            },
            details: {
                display: 'flex',
                flexDirection: 'column',
            },
            content: {
                flex: '1 0 auto',
            },
            controls: {
                display: 'flex',
                alignItems: 'center',
                paddingLeft: theme.spacing(1),
                paddingBottom: theme.spacing(1),
            },
            orange: {
                color: theme.palette.getContrastText(deepOrange[500]),
                backgroundColor: deepOrange[500],
            },
            purple: {
                color: theme.palette.getContrastText(deepPurple[500]),
                backgroundColor: deepPurple[500],
            },
        }));

        let state = this.props.state.state;

        let currentUser = state.users[state.currentUserIndex];
        let task = this.props.obj.task;

        let taskTestingDtls = null;

        if (currentUser !== undefined && currentUser !== null) {
            taskTestingDtls = currentUser.notifications[task.id]
        }

        let taskManager = state.users.find(x => x['address'] === task.managerAddress) || genericUser;
        let taskManagerUsername = taskManager.name;
        if (!taskManager) {
            taskManager = genericUser
        }
        let taskFreelancer = state.users.find(x => x['address'] === task.freelancerAddress) || genericUser;
        let taskFreelancerUsername = taskFreelancer.name ? taskFreelancer.name : " ";

        let taskAssessor = state.users.find(x => x['address'] === task.assessorAddress) || genericUser;
        let taskAssessorUsername = taskAssessor.name;

        let currentTaskAssessor = state.users.find(x => x['address'] === task.assessorAddress);

        let emptyAddress = "0x0000000000000000000000000000000000000000";

        let taskStatus = TaskStatus.OPEN;
        let backgroundColorStatus = '#ff8a80';
        let taskStatusString = "Open";
        if (task.isDone) {
            taskStatusString = "Done";
            taskStatus = TaskStatus.DONE;
            backgroundColorStatus = '#00e676';
        } else if (task.isReady) {
            taskStatusString = "Ready";
            taskStatus = TaskStatus.READY;
            backgroundColorStatus = '#2962ff';
        } else if (task.freelancerAddress && task.freelancerAddress !== emptyAddress
            && task.assessorAddress && task.assessorAddress !== emptyAddress) {
            taskStatusString = "In Progress";
            taskStatus = TaskStatus.IN_PROGRESS;
            backgroundColorStatus = '#ff9800';
        }

        let allFRDivs = [];
        for (const [index, value] of task.freelancers.entries()) {
            let thatUser = state.users.find(x => x['address'] === value);
            if (thatUser === undefined || thatUser === null) {
                thatUser = genericUser;
            }
            allFRDivs.push(
                <ListItem key={index.toString() + value} alignItems="flex-start">
                    <ListItemAvatar>
                        <Avatar style={{backgroundColor: "#009688", color: "white"}}>{thatUser.name ?
                            thatUser.name.substr(0, 1) : " "}</Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={thatUser.name}
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    color="textPrimary"
                                >
                                    {value}
                                </Typography>
                            </React.Fragment>
                        }
                    />
                </ListItem>
            );
        }

        let allASDivs = [];
        for (const [index, value] of task.assessors.entries()) {
            let thatUser = state.users.find(x => x['address'] === value);
            if (thatUser === undefined || thatUser === null) {
                thatUser = genericUser;
            }
            allASDivs.push(
                <ListItem alignItems="flex-start">
                    <ListItemAvatar>
                        <Avatar style={{backgroundColor: "#2196f3", color: "white"}}>{thatUser.name ?
                            thatUser.name.substr(0, 1) : " "}</Avatar>
                    </ListItemAvatar>
                    <ListItemText
                        primary={thatUser.name}
                        secondary={
                            <React.Fragment>
                                <Typography
                                    component="span"
                                    variant="body2"
                                    color="textPrimary"
                                >
                                    {value}
                                </Typography>
                            </React.Fragment>
                        }
                    />
                </ListItem>
            );
        }

        let acceptTaskButton = null, editTask = null, cancelTask = null, finishTask = null, rejectTask = null,
            startTask = null, taskTestingDetails = null, assessorIsNotResponding = null,
            addAssessorToTask = null, applyToFreelanceTask = null, acceptToEvaluateTask = null, arbitrateTask = null;

        let currentUserType = currentUser === undefined ? undefined : currentUser.type;

        switch (taskStatus) {
            case "Open": {
                switch (currentUserType) {
                    case "Manager": {
                        if (task.managerAddress === currentUser.address) {
                            if (task.freelancers.length > 0 && task.assessors.length > 0) {
                                startTask = <StartTaskForm name="Start Task" state={state} task={task}
                                                           onTaskSucces={this.props.onTaskSucces}
                                                           onSucces={this.props.onSucces}
                                                           onLoading={this.props.onLoading}
                                                           showAlert={this.props.showAlert}
                                                           disabled={false} color='primary'>
                                </StartTaskForm>;
                            }
                            if (task.freelancers.length === 0) {
                                editTask = <EditTaskForm name="Edit Task" state={state} task={task}
                                                         onTaskSucces={this.props.onTaskSucces}
                                                         onLoading={this.props.onLoading}
                                                         onSucces={this.props.onSucces}
                                                         showAlert={this.props.showAlert}
                                                         disabled={false} color='primary'>
                                </EditTaskForm>;
                            }
                            if (task.assessorAddress === "0x0000000000000000000000000000000000000000") {
                                addAssessorToTask = <AddAssessorForm name="Add assessor" state={state} task={task}
                                                                     onLoading={this.props.onLoading}
                                                                     onTaskSucces={this.props.onTaskSucces}
                                                                     onSucces={this.props.onSucces}
                                                                     showAlert={this.props.showAlert}
                                                                     disabled={false} color='default'>
                                </AddAssessorForm>;
                            }

                            cancelTask = <CancelTaskForm name="Cancel Task" state={state} task={task}
                                                         onTaskSucces={this.props.onTaskSucces}
                                                         onLoading={this.props.onLoading}
                                                         onSucces={this.props.onSucces}
                                                         showAlert={this.props.showAlert}
                                                         disabled={false} color='secondary'>
                            </CancelTaskForm>;
                        }
                        break;
                    }
                    case "Freelancer": {
                        let taskHasAssessor = task.assessorAddress !== undefined
                            && task.assessorAddress !== null
                            && task.assessorAddress !== "0x0000000000000000000000000000000000000000";
                        let imIn = taskHasAssessor && task.freelancers.indexOf(currentUser.address) === -1
                            && task.expertiseType === currentUser.expertiseType;

                        if (imIn) {
                            let applyButtonType = task.freelancerAddress === currentUser.address ? 'disabled' : 'primary';
                            applyToFreelanceTask = <ApplyToWorkOnTaskForm name="Apply to work" state={state} task={task}
                                                                          onLoading={this.props.onLoading}
                                                                          onTaskSucces={this.props.onTaskSucces}
                                                                          onSucces={this.props.onSucces}
                                                                          showAlert={this.props.showAlert}
                                                                          disabled={false} color={applyButtonType}>
                            </ApplyToWorkOnTaskForm>;
                        }
                        break;
                    }
                    case "Assessor": {
                        let imIn = task.assessorAddress !== currentUser.address && task.assessors.indexOf(currentUser.address) !== -1;
                        if (imIn) {
                            let applyButtonType = task.assessorAddress === currentUser.address ? 'disabled' : 'primary';
                            acceptToEvaluateTask =
                                <AcceptToEvaluateForm name="Accept to evaluate" state={state} task={task}
                                                      onTaskSucces={this.props.onTaskSucces}
                                                      onLoading={this.props.onLoading}
                                                      onSucces={this.props.onSucces}
                                                      showAlert={this.props.showAlert}
                                                      disabled={false} color={applyButtonType}>
                                </AcceptToEvaluateForm>;
                        }
                        break;
                    }
                }
                break;
            }
            case "In progress": {
                if (currentUserType === "Freelancer" && task.freelancerAddress === currentUser.address) {
                    finishTask = <FinishTaskForm name="Finish task" state={state} task={task}
                                                 onTaskSucces={this.props.onTaskSucces}
                                                 onLoading={this.props.onLoading}
                                                 onSucces={this.props.onSucces}
                                                 showAlert={this.props.showAlert}
                                                 disabled={false} color='primary'>
                    </FinishTaskForm>;
                }
                break;
            }
            case "Ready": {
                switch (currentUserType) {
                    case "Manager": {
                        if (task.managerAddress === currentUser.address &&
                            currentUser.notifications[task.id]) {
                            if (currentTaskAssessor.notifications[task.id] === undefined) {
                                acceptTaskButton = <AcceptTaskForm name="Accept Task" state={state} task={task}
                                                                   onTaskSucces={this.props.onTaskSucces}
                                                                   onLoading={this.props.onLoading}
                                                                   onSucces={this.props.onSucces}
                                                                   showAlert={this.props.showAlert}
                                                                   disabled={false} color='primary'>
                                </AcceptTaskForm>;
                                rejectTask = <RejectTaskForm name="Reject Task" state={state} task={task}
                                                             onTaskSucces={this.props.onTaskSucces}
                                                             onLoading={this.props.onLoading}
                                                             onSucces={this.props.onSucces}
                                                             showAlert={this.props.showAlert}
                                                             disabled={false} color='secondary'>
                                </RejectTaskForm>;
                            } else {

                                let taskTimeArray = task.timeArray;
                                let len = taskTimeArray.length;
                                if(len === 5 && Number(taskTimeArray[0]) + Number(taskTimeArray[3]) < Math.floor(new Date() / 1000)) {

                                    assessorIsNotResponding = <AssessorIsNotRespondingForm name="Disolve Task" state={state} task={task}
                                                                                           onTaskSucces={this.props.onTaskSucces}
                                                                                           onLoading={this.props.onLoading}
                                                                                           onSucces={this.props.onSucces}
                                                                                           showAlert={this.props.showAlert}
                                                                                           disabled={false} color='secondary'>
                                    </AssessorIsNotRespondingForm>;
                                }
                            }


                            taskTestingDetails = <div className="row" id="detailsBoxMiddle">
                                <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Task testing
                                    details:</p>
                                <p style={{fontSize: "16px"}}>{currentUser.notifications[task.id]}</p>
                            </div>;

                        }
                        break;
                    }
                    case "Assessor": {
                        if (task.assessorAddress === currentUser.address &&
                            currentUser.notifications[task.id] !== undefined) {
                            arbitrateTask = <ArbitrateTaskForm name="Arbitrate task" task={task} state={state}
                                                               onTaskSucces={this.props.onTaskSucces}
                                                               onLoading={this.props.onLoading}
                                                               onSucces={this.props.onSucces}
                                                               showAlert={this.props.showAlert}
                                                               disabled={false} color='primary'>
                            </ArbitrateTaskForm>;
                            taskTestingDetails = <div className="row" id="detailsBoxMiddle">
                                <p style={{fontSize: "16px", color: "blue", marginRight: "5px"}}>Task testing
                                    details:</p>
                                <p style={{fontSize: "16px"}}>{currentUser.notifications[task.id]}</p>
                            </div>;
                        }
                        break;
                    }
                }
                break;
            }
            case "Done": {
                break;
            }
        }

        let displayDate = "none";
        let countdownType;

        // 0 : evaluationTime in seconds
        // 1 : freelanceTime in seconds
        // 2 : starting time
        // 3 : finish time
        // 4 : accept / reject time
        // 5 : evaluation time
        let countdownDateAsString;
        if (taskStatusString === "Ready" || taskStatusString === "In Progress") {

            let taskTimeArray = task.timeArray;
            let len = taskTimeArray.length;
            const currentTimestamp = Math.floor(new Date() / 1000);
            let referenceTimestamp = 0;
            switch (len) {
                case 3: {

                    referenceTimestamp = Number(taskTimeArray[1]) + Number(taskTimeArray[2]);
                    if (referenceTimestamp < currentTimestamp) {
                        countdownType = "Execution time expired! The freelancer will be punished.";
                        displayDate = "none";
                    } else {
                        countdownType = "Execution countdown";
                        displayDate = "block";
                    }
                    break;
                }
                case 4: {
                    countdownType = "Task finished in: "
                        + this.getSecondsAsString(Number(taskTimeArray[3]) - Number(taskTimeArray[2]))
                        + " | Limit was: " + this.getSecondsAsString(Number(taskTimeArray[1]));
                    referenceTimestamp = currentTimestamp;
                    break;
                }
                case 5: {

                    referenceTimestamp = Number(taskTimeArray[0]) + Number(taskTimeArray[4]);
                    if (referenceTimestamp < currentTimestamp) {
                        countdownType = "Evaluation time expired! The assessor will be punished.";
                        displayDate = "none";
                    } else {
                        countdownType = "Evaluation countdown";
                        displayDate = "block";
                    }
                    break;
                }
            }

            let dateFormatted = new Date(referenceTimestamp * 1000).toISOString();
            countdownDateAsString = dateFormatted;
        } else if (taskStatusString === "Done") {
            let taskTimeArray = task.timeArray;
            let len = taskTimeArray.length;
            if (len === 5) {
                countdownType = "Task completed in: "
                    + this.getSecondsAsString(Number(taskTimeArray[4]) - Number(taskTimeArray[2]));
            }
            if (len === 6) {
                countdownType = "Task completed in: "
                    + this.getSecondsAsString(Number(taskTimeArray[5]) - Number(taskTimeArray[2]));
            }
        }
        let finalDiv = null;

        if (task.id > 0) {
            finalDiv = <Card className={classes.card} style={{marginBottom: "1%"}}>
                <div className={classes.details} style={{marginLeft: "2.5%", width: '95%'}}>
                    <div className="row" style={{}}>
                        <div style={{float: "left", width: "55%"}}>
                            <CardHeader
                                avatar={
                                    <Avatar aria-label="recipe"
                                            style={{backgroundColor: "red", color: "white"}}>
                                        {taskManagerUsername === undefined ? " " : taskManagerUsername.substr(0, 1)}
                                    </Avatar>
                                }
                                title={taskManagerUsername}
                                subheader={taskManager.address}
                            />
                        </div>
                        <div style={{float: "right", width: "44%", marginTop: "10px"}}>
                            <div style={{float: "left", width: "60%"}}>
                                <Chip
                                    style={{float: "left", marginLeft: "0px", backgroundColor: "white"}}
                                    avatar={<Avatar>{task.expertiseType.substr(0, 1)}</Avatar>}
                                    label={task.expertiseType}
                                    clickable
                                    color="primary"
                                    variant="outlined"
                                />

                                <Chip
                                    style={{float: "left", marginLeft: "15px", backgroundColor: backgroundColorStatus}}
                                    avatar={<Avatar> </Avatar>}
                                    label={taskStatusString}
                                    clickable
                                    color="secondary"
                                />
                            </div>
                            <div style={{float: "right", marginRight: "0px"}}>
                                <Typography style={{margin: "0px", padding: "0px"}} gutterBottom>
                                    {countdownType}
                                </Typography>
                                <div style={{display: displayDate}}>
                                    <Countdown style={{float: "right", marginRight: "0px"}}
                                               date={countdownDateAsString}
                                    />
                                </div>
                            </div>
                        </div>
                    </div>
                    <CardActionArea>
                        <CardContent>
                            <Typography style={{marginBottom: "5px"}} gutterBottom variant="h6" component="h6">
                                Task {task.id}: {task.description}
                            </Typography>
                            <div className="row" style={{marginLeft: "5px"}}>
                                <div style={{float: "left", width: "49%"}}>
                                    <Typography variant="overline" gutterBottom>
                                        Freelancer reward: {task.freelancerReward} Te Tai Tokens
                                    </Typography>
                                    <CardHeader
                                        avatar={
                                            <Avatar aria-label="recipe"
                                                    style={{backgroundColor: "#00695c", color: "white"}}>
                                                {taskFreelancerUsername.substr(0, 1)}
                                            </Avatar>
                                        }
                                        title={taskFreelancerUsername}
                                        subheader={taskFreelancer.address}
                                    />
                                </div>
                                <div style={{float: "right", width: "49%"}}>
                                    <Typography variant="overline" gutterBottom>
                                        Assessor reward: {task.assessorReward} Te Tai Tokens
                                    </Typography>
                                    <CardHeader
                                        avatar={
                                            <Avatar aria-label="recipe"
                                                    style={{backgroundColor: "#1565c0", color: "white"}}>
                                                {taskAssessorUsername ? taskAssessorUsername.substr(0, 1) : " "}
                                            </Avatar>
                                        }
                                        title={taskAssessorUsername}
                                        subheader={taskAssessor.address}
                                    />
                                </div>
                            </div>

                        </CardContent>
                    </CardActionArea>
                    <div className="row" style={{marginLeft: "2%", marginTop: "1%"}}>
                        <div style={{float: "left", width: "49%"}}>
                            <ExpansionPanel>
                                <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon/>}
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                >
                                    <Typography className={classes.heading}>Freelancers interested</Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <List className={classes.root}>
                                        {allFRDivs}
                                    </List>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        </div>
                        <div style={{float: "right", width: "49%"}}>
                            <ExpansionPanel>
                                <ExpansionPanelSummary
                                    expandIcon={<ExpandMoreIcon/>}
                                    aria-controls="panel2a-content"
                                    id="panel2a-header"
                                >
                                    <Typography className={classes.heading}>Assessors wanted</Typography>
                                </ExpansionPanelSummary>
                                <ExpansionPanelDetails>
                                    <List className={classes.root}>
                                        {allASDivs}
                                    </List>
                                </ExpansionPanelDetails>
                            </ExpansionPanel>
                        </div>
                    </div>
                    <div style={{float: "left"}}>
                        <div style={{
                            display:
                                (taskTestingDtls !== undefined && taskTestingDtls !== null
                                    && taskTestingDtls.length > 0) ? "block" : "none"
                        }}>
                            <Typography style={{marginBottom: "3%", marginTop: "3%"}}>
                                Task testing details: {taskTestingDtls}
                            </Typography>
                        </div>
                    </div>
                    <CardActions style={{float: "right"}}>
                        {acceptTaskButton}
                        {acceptToEvaluateTask}
                        {addAssessorToTask}
                        {applyToFreelanceTask}
                        {arbitrateTask}
                        {editTask}
                        {finishTask}
                        {rejectTask}
                        {startTask}
                        {cancelTask}
                        {assessorIsNotResponding}
                    </CardActions>
                </div>

            </Card>
        }

        return (
            <div>
                {finalDiv}
            </div>
        );
    }

}

export default TaskCard;

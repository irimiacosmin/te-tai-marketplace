import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import SnackbarComponent from "./SnackbarComponent";
import App from "../App";

const UserType = {
    MANAGER: 'Manager',
    FREELANCER: 'Freelancer',
    ASSESSOR: 'Assessor'
};


class AddAssessorForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonName: props.name,
            disabledValue: props.disabled,
            isManager: true,
            userTypeIndex: 0,
            expertiseTypeIndex: 1,
            open: false,
            task: this.props.task,
            assessors: this.props.state.users.filter(assessor => assessor.type === UserType.ASSESSOR
                && this.props.task.assessors.indexOf(assessor.address) === -1)
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleAddAssessor = this.handleAddAssessor.bind(this);
        this.handleExpertiseChange = this.handleExpertiseChange.bind(this);

    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleAddAssessor = () => {
        let globalThis = this;
        let task = this.state.task;
        let assessor = this.state.assessors[this.state.userTypeIndex];
        let currentUser = this.props.state.users[this.props.state.currentUserIndex];
        globalThis.props.onLoading(true);
        globalThis.handleClose();

        this.props.state.contract.methods.addAssessorToTask(task.id, assessor.address)
            .send({from: currentUser.address, gas: 20000000}, function (error, result) {
                if (error === null) {
                    globalThis.props.onTaskSucces(globalThis.props.state.contract);
                    globalThis.props.showAlert("Assessor added successfully", "success");

                }
                if (result === undefined) {
                    let message = App.getMessageFromError(error);
                    if (message.length < 1) {
                        message = "Error while adding assessor to task.";
                    }
                    globalThis.props.showAlert(message, "error");
                }
                globalThis.props.onLoading(false);
            });
    };

    handleChange(event) {
        let val = event.target.value;
        this.setState({userTypeIndex: val, isManager: val === 0});
    }

    handleExpertiseChange(event) {
        this.setState({expertiseTypeIndex: event.target.value});
    }

    render() {
        let currentTask = this.props.task;

        let assessors = [];
        this.state.assessors.forEach((assessor, index) => {
            if(assessor.expertiseType === currentTask.expertiseType) {
                assessors.push(<MenuItem value={index}>{App.getUserNameWithSubAdress(assessor)}</MenuItem>);
            }
        });

        let message = null;
        if (this.state.showMessage) {
            message = <SnackbarComponent text={this.state.messageText} showMessage={this.state.showMessage}
                                         handleClose={this.handleSnackbarClose}
                                         severity={this.state.messageSeverity}></SnackbarComponent>
        }

        return (
            <div style={{margin: "10px", marginTop: "15px"}}>
                {message}
                <Button variant="outlined" color="primary" disabled={this.props.disabled}
                        onClick={this.handleClickOpen}>
                    {this.state.buttonName}
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Add Assessor to Task</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            Please select one of the available assessors.
                        </DialogContentText>
                        <div style={{marginTop: "5%"}}>
                            <FormControl style={{marginRight: "5%", width: "300px"}}>
                                <InputLabel id="demo-simple-select-label">Assessors available</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.userTypeIndex}
                                    onChange={this.handleChange}
                                >
                                    {assessors}
                                </Select>
                            </FormControl>

                        </div>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleAddAssessor} color="primary">
                            Add Assessor
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default AddAssessorForm;

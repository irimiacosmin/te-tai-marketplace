import React, {Component} from 'react';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FormControl from "@material-ui/core/FormControl";
import InputLabel from "@material-ui/core/InputLabel";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import SnackbarComponent from "./SnackbarComponent";
import App from "../App";


class RegisterForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            buttonName: props.name,
            disabledValue: props.disabled,
            tariffValue: null,
            nameValue: null,
            isManager: true,
            userTypeIndex: 0,
            expertiseTypeIndex: 1,
            open: false,
            showMessage: false,
            messageText: null,
            messageSeverity: null
        };
        this.handleClickOpen = this.handleClickOpen.bind(this);
        this.handleClose = this.handleClose.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleRegister = this.handleRegister.bind(this);
        this.handleExpertiseChange = this.handleExpertiseChange.bind(this);
        this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
        this.showAlert = this.showAlert.bind(this);
    }

    handleClickOpen = () => {
        this.setState({open: true});
    };

    handleClose = () => {
        this.setState({open: false});
    };

    handleTarrifChange = (e) => {
        this.setState({tariffValue: e.target.value});
    };

    handleNameChange = (e) => {
        this.setState({nameValue: e.target.value});
    };

    showAlert(text, severity) {
        this.setState({
            showMessage: true,
            messageText: text,
            messageSeverity: severity
        });
    }

    handleRegister = () => {
        let globalThis = this;

        let contract = this.props.state.contract;
        let currentUser = this.props.state.users[this.props.state.currentUserIndex];

        let nameValue = this.state.nameValue;
        if (nameValue === null || nameValue.length < 3) {
            globalThis.showAlert("Name must be greater than 3 characters", "warning");
            return;
        }
        if (this.state.isManager) {
            contract.methods.registerManager(nameValue).send(
                {from: currentUser.address, gas: 20000000}, function (error, result) {
                    if (error === null) {
                        globalThis.showAlert("Manager registered successfully", "success");
                        globalThis.props.onSucces(globalThis.props.state.addresses, globalThis.props.state.contract);
                    }
                    if (result === undefined) {
                        let message = App.getMessageFromError(error);
                        if (message.length < 1) {
                            message = "Registration failed! Please try again later.";
                        }
                        globalThis.props.showAlert(message, "error");
                    }
                });
            globalThis.handleClose();
            return;
        }
        let userType = this.state.userTypeIndex;
        let tariffValue = this.state.tariffValue;
        if (tariffValue === null || tariffValue < 1) {
            globalThis.showAlert("Tariff must be greater than 0", "warning");
            return;
        }

        switch (userType) {
            case 1: {
                contract.methods.registerFreelancer(nameValue, tariffValue, this.state.expertiseTypeIndex).send(
                    {from: currentUser.address, gas: 20000000}, function (error, result) {
                        if (error === null) {
                            globalThis.props.onSucces(globalThis.props.state.addresses, globalThis.props.state.contract);
                            globalThis.showAlert("Freelancer registered successfully", "success");
                        }
                        if (result === undefined) {
                            let message = App.getMessageFromError(error);
                            if (message.length < 1) {
                                message = "Registration failed! Please try again later.";
                            }
                            globalThis.props.showAlert(message, "error");
                        }

                    });
                break;
            }
            case 2: {
                contract.methods.registerAssesor(nameValue, tariffValue, this.state.expertiseTypeIndex).send(
                    {from: currentUser.address, gas: 20000000}, function (error, result) {
                        if (error === null) {
                            globalThis.props.onSucces(globalThis.props.state.addresses, globalThis.props.state.contract);
                            globalThis.showAlert("Assessor registered successfully", "success");
                        }
                        if (result === undefined) {
                            let message = App.getMessageFromError(error);
                            if (message.length < 1) {
                                message = "Registration failed! Please try again later.";
                            }
                            globalThis.props.showAlert(message, "error");
                        }

                    });
                break;
            }
            default:
                break;
        }
        globalThis.handleClose();
    };

    handleChange(event) {
        let val = event.target.value;
        this.setState({userTypeIndex: val, isManager: val === 0});
    }

    handleExpertiseChange(event) {
        this.setState({expertiseTypeIndex: event.target.value});
    }

    handleSnackbarClose() {
        this.setState({showMessage: false});
    }

    render() {
        // let userTypes = this.props.state.UserType;
        // let expertiseTypes = this.props.state.ExpertiseType;
        // for(let i = 0; i< userTypes.length; i++) {
        //     console.log(userTypes[i]);
        // }

        let message = null;
        if (this.state.showMessage) {
            message = <SnackbarComponent text={this.state.messageText} showMessage={this.state.showMessage}
                                         handleClose={this.handleSnackbarClose}
                                         severity={this.state.messageSeverity}/>
        }

        return (
            <div>
                {message}
                <Button variant="contained" disabled={this.props.disabled}
                        onClick={this.handleClickOpen}>
                    {this.state.buttonName}
                </Button>
                <Dialog open={this.state.open} onClose={this.handleClose} aria-labelledby="form-dialog-title">
                    <DialogTitle id="form-dialog-title">Register to Smart Hitman</DialogTitle>
                    <DialogContent>
                        <DialogContentText>
                            To unlock all the features of the DAPP you must Register first.
                            Please select what type of user you want to be and select your name.
                        </DialogContentText>
                        <div style={{marginTop: "5%"}}>
                            <FormControl style={{marginRight: "5%"}}>
                                <InputLabel id="demo-simple-select-label">User type</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.userTypeIndex}
                                    onChange={this.handleChange}
                                >
                                    <MenuItem value={0}>Manager</MenuItem>
                                    <MenuItem value={1}>Freelancer</MenuItem>
                                    <MenuItem value={2}>Assessor</MenuItem>
                                </Select>
                            </FormControl>

                            <FormControl style={{width: "150px", marginRight: "5%"}}
                                         disabled={this.state.isManager}
                            >
                                <InputLabel id="demo-simple-select-label" style={{width: "150px"}}>
                                    Expertise Type</InputLabel>
                                <Select
                                    labelId="demo-simple-select-label"
                                    id="demo-simple-select"
                                    value={this.state.expertiseTypeIndex}
                                    onChange={this.handleExpertiseChange}
                                    style={{width: "150px"}}
                                >
                                    <MenuItem value={1}>Killer</MenuItem>
                                    <MenuItem value={2}>Debt collector</MenuItem>
                                </Select>
                            </FormControl>

                            <TextField style={{width: "150px", marginRight: "5%"}}
                                       disabled={this.state.isManager}
                                       onChange={this.handleTarrifChange}
                                       id="standard-number"
                                       label="Tariff"
                                       type="number"
                                       InputLabelProps={{
                                           shrink: true,
                                       }}
                            />
                        </div>

                        <TextField
                            onChange={this.handleNameChange}
                            autoFocus
                            margin="dense"
                            id="name"
                            label="Name"
                            type="email"
                            fullWidth
                        />

                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                            Cancel
                        </Button>
                        <Button onClick={this.handleRegister} color="primary">
                            Register
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        )
    }
}

export default RegisterForm;

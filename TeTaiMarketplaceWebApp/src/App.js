import React, {Component} from 'react';
import './App.css';
import RegisterForm from './Components/RegisterForm'
import ResponseBox from './Components/ResponseBox'
import Button from '@material-ui/core/Button';
import WebsiteLogo from './logo/shoot.png'
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import CreateTaskForm from "./Components/CreateTaskForm";
import Web3 from "web3";
import SnackbarComponent from "./Components/SnackbarComponent";
import Chip from "@material-ui/core/Chip";
import AttachMoneyRoundedIcon from '@material-ui/icons/AttachMoneyRounded';
import StarRoundedIcon from '@material-ui/icons/StarRounded';
import WorkRoundedIcon from '@material-ui/icons/WorkRounded';
import MoneyRoundedIcon from '@material-ui/icons/MoneyRounded';
import Badge from "@material-ui/core/Badge";
import MailIcon from '@material-ui/icons/Mail';
import Menu from '@material-ui/core/Menu';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import withStyles from "@material-ui/core/styles/withStyles";
import NotificationsActiveIcon from '@material-ui/icons/NotificationsActive';
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import Typography from "@material-ui/core/Typography";

const UserType = {
    MANAGER: 'Manager',
    FREELANCER: 'Freelancer',
    ASSESSOR: 'Assessor'
};

const ExpertiseType = {
    NONE: 'None',
    KILLER: 'Killer',
    DEBT_COLLECTOR: 'Debt collector',
};

const TaskStatus = {
    OPEN: 'Open',
    IN_PROGRESS: 'In progress',
    READY: 'Ready',
    DONE: 'Done'
};

class App extends Component {
    constructor(props) {
        super(props);
        let abi = [
            {
                "inputs": [
                    {
                        "internalType": "address",
                        "name": "_addr",
                        "type": "address"
                    }
                ],
                "payable": true,
                "stateMutability": "payable",
                "type": "constructor"
            },
            {
                "anonymous": false,
                "inputs": [
                    {
                        "indexed": false,
                        "internalType": "uint256",
                        "name": "amount",
                        "type": "uint256"
                    },
                    {
                        "indexed": false,
                        "internalType": "address",
                        "name": "fromAccount",
                        "type": "address"
                    },
                    {
                        "indexed": false,
                        "internalType": "uint256",
                        "name": "totalBalance",
                        "type": "uint256"
                    }
                ],
                "name": "TeTaiReceivedTokens",
                "type": "event"
            },
            {
                "anonymous": false,
                "inputs": [
                    {
                        "indexed": false,
                        "internalType": "uint256",
                        "name": "amount",
                        "type": "uint256"
                    },
                    {
                        "indexed": false,
                        "internalType": "address",
                        "name": "fromAccount",
                        "type": "address"
                    },
                    {
                        "indexed": false,
                        "internalType": "uint256",
                        "name": "totalBalance",
                        "type": "uint256"
                    }
                ],
                "name": "TeTaiSendTokens",
                "type": "event"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "address",
                        "name": "",
                        "type": "address"
                    }
                ],
                "name": "_assessors",
                "outputs": [
                    {
                        "internalType": "bool",
                        "name": "_exists",
                        "type": "bool"
                    },
                    {
                        "internalType": "string",
                        "name": "_name",
                        "type": "string"
                    },
                    {
                        "internalType": "address",
                        "name": "_address",
                        "type": "address"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_reputation",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_tariff",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_tokens",
                        "type": "uint256"
                    },
                    {
                        "internalType": "enum TeTaiMarketplace.ExpertiseType",
                        "name": "_expertiseType",
                        "type": "uint8"
                    },
                    {
                        "internalType": "enum TeTaiMarketplace.UserType",
                        "name": "_userType",
                        "type": "uint8"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "_assessorsCount",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "address",
                        "name": "",
                        "type": "address"
                    }
                ],
                "name": "_freelancers",
                "outputs": [
                    {
                        "internalType": "bool",
                        "name": "_exists",
                        "type": "bool"
                    },
                    {
                        "internalType": "string",
                        "name": "_name",
                        "type": "string"
                    },
                    {
                        "internalType": "address",
                        "name": "_address",
                        "type": "address"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_reputation",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_tariff",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_tokens",
                        "type": "uint256"
                    },
                    {
                        "internalType": "enum TeTaiMarketplace.ExpertiseType",
                        "name": "_expertiseType",
                        "type": "uint8"
                    },
                    {
                        "internalType": "enum TeTaiMarketplace.UserType",
                        "name": "_userType",
                        "type": "uint8"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "_freelancersCount",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "address",
                        "name": "",
                        "type": "address"
                    }
                ],
                "name": "_managers",
                "outputs": [
                    {
                        "internalType": "bool",
                        "name": "_exists",
                        "type": "bool"
                    },
                    {
                        "internalType": "string",
                        "name": "_name",
                        "type": "string"
                    },
                    {
                        "internalType": "address",
                        "name": "_address",
                        "type": "address"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_reputation",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_tariff",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_tokens",
                        "type": "uint256"
                    },
                    {
                        "internalType": "enum TeTaiMarketplace.ExpertiseType",
                        "name": "_expertiseType",
                        "type": "uint8"
                    },
                    {
                        "internalType": "enum TeTaiMarketplace.UserType",
                        "name": "_userType",
                        "type": "uint8"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "_managersCount",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "_money",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "name": "_tasks",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "_taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "string",
                        "name": "_description",
                        "type": "string"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_freelancerReward",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "_assessorReward",
                        "type": "uint256"
                    },
                    {
                        "internalType": "enum TeTaiMarketplace.ExpertiseType",
                        "name": "_expertiseType",
                        "type": "uint8"
                    },
                    {
                        "internalType": "uint8",
                        "name": "_timeLen",
                        "type": "uint8"
                    },
                    {
                        "internalType": "address",
                        "name": "_managerAddress",
                        "type": "address"
                    },
                    {
                        "internalType": "address",
                        "name": "_freelancerAddress",
                        "type": "address"
                    },
                    {
                        "internalType": "address",
                        "name": "_assessorAddress",
                        "type": "address"
                    },
                    {
                        "internalType": "bool",
                        "name": "isReady",
                        "type": "bool"
                    },
                    {
                        "internalType": "bool",
                        "name": "isDone",
                        "type": "bool"
                    },
                    {
                        "internalType": "string",
                        "name": "_taskTestingDetails",
                        "type": "string"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "_tasksCount",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [],
                "name": "getTime",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [],
                "name": "payTimeMachine",
                "outputs": [],
                "payable": true,
                "stateMutability": "payable",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [],
                "name": "getBalanceOfTimeMachine",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "index",
                        "type": "uint256"
                    }
                ],
                "name": "getTimeFromTask",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "address",
                        "name": "addr",
                        "type": "address"
                    }
                ],
                "name": "getFreelancerFromTask",
                "outputs": [
                    {
                        "internalType": "bool",
                        "name": "",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "address",
                        "name": "addr",
                        "type": "address"
                    }
                ],
                "name": "getAssessorFromTask",
                "outputs": [
                    {
                        "internalType": "bool",
                        "name": "",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "address",
                        "name": "userAddr",
                        "type": "address"
                    },
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "userTypeValue",
                        "type": "uint256"
                    }
                ],
                "name": "getNotificationFromUser",
                "outputs": [
                    {
                        "internalType": "string",
                        "name": "",
                        "type": "string"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": true,
                "inputs": [
                    {
                        "internalType": "address",
                        "name": "_addr",
                        "type": "address"
                    }
                ],
                "name": "getBalanceOf",
                "outputs": [
                    {
                        "internalType": "uint256",
                        "name": "",
                        "type": "uint256"
                    }
                ],
                "payable": false,
                "stateMutability": "view",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "string",
                        "name": "name",
                        "type": "string"
                    },
                    {
                        "internalType": "uint256",
                        "name": "tariff",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "expertiseTypeValue",
                        "type": "uint256"
                    }
                ],
                "name": "registerFreelancer",
                "outputs": [
                    {
                        "internalType": "bool",
                        "name": "",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "string",
                        "name": "name",
                        "type": "string"
                    },
                    {
                        "internalType": "uint256",
                        "name": "tariff",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "expertiseTypeValue",
                        "type": "uint256"
                    }
                ],
                "name": "registerAssesor",
                "outputs": [
                    {
                        "internalType": "bool",
                        "name": "",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "string",
                        "name": "name",
                        "type": "string"
                    }
                ],
                "name": "registerManager",
                "outputs": [
                    {
                        "internalType": "bool",
                        "name": "",
                        "type": "bool"
                    }
                ],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "string",
                        "name": "description",
                        "type": "string"
                    },
                    {
                        "internalType": "uint256",
                        "name": "freelancerReward",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "assessorReward",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "expertiseTypeValue",
                        "type": "uint256"
                    }
                ],
                "name": "addTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "string",
                        "name": "description",
                        "type": "string"
                    },
                    {
                        "internalType": "uint256",
                        "name": "freelancerReward",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "assessorReward",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "expertiseTypeValue",
                        "type": "uint256"
                    }
                ],
                "name": "editTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "timeEstimateHours",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "timeEstimateMinutes",
                        "type": "uint256"
                    }
                ],
                "name": "acceptToEvaluateTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "address",
                        "name": "assessorAddress",
                        "type": "address"
                    }
                ],
                "name": "addAssessorToTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "timeEstimateHours",
                        "type": "uint256"
                    },
                    {
                        "internalType": "uint256",
                        "name": "timeEstimateMinutes",
                        "type": "uint256"
                    }
                ],
                "name": "applyToFreelanceTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "address",
                        "name": "freelancerAddress",
                        "type": "address"
                    }
                ],
                "name": "startTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    }
                ],
                "name": "cancelTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "string",
                        "name": "taskTestingDetails",
                        "type": "string"
                    }
                ],
                "name": "finishTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    }
                ],
                "name": "acceptTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    }
                ],
                "name": "rejectTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    }
                ],
                "name": "dissolveTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            },
            {
                "constant": false,
                "inputs": [
                    {
                        "internalType": "uint256",
                        "name": "taskId",
                        "type": "uint256"
                    },
                    {
                        "internalType": "bool",
                        "name": "taskIsComplete",
                        "type": "bool"
                    }
                ],
                "name": "arbitrateTask",
                "outputs": [],
                "payable": false,
                "stateMutability": "nonpayable",
                "type": "function"
            }
        ];

        var web3 = new Web3(new Web3.providers.HttpProvider('http://localhost:8545'));


        this.state = {
            contractAddress: "0x1F9Af13E36eF2399e41b8303d19b2ac0B8Dcc897",
            emptyAddress: "0x0000000000000000000000000000000000000000",
            timeMachineBalance: 0,
            addresses: [],
            contract: null,
            abi: abi,
            users: [],
            tasks: [],
            toShowTasks: [],
            UserType: UserType,
            ExpertiseType: ExpertiseType,
            currentUserIndex: 0,
            showMessage: false,
            messageText: null,
            messageSeverity: null,
            anchorEl: null,
            expertiseTypeIndex: 0,
            taskStatusIndex: 0,
            checkedB: false,
            loadingBar: "flex",
            application: "block",
            loadingGifs: [
                "https://i.gifer.com/XOsX.gif",
                "https://i.gifer.com/origin/26/264162db570a4614c8fd7dc15c757b8e_w200.webp",
                "https://i.gifer.com/origin/c5/c5db38375f8f93e17b30049a63f15c81_w200.webp",
                "https://i.gifer.com/origin/9b/9be3cc63d40d8ea231322e87d6aab7ca_w200.webp"
            ]
        };


        let contract = new web3.eth.Contract(abi, this.state.contractAddress);

        this.getNameOrAddress = this.getNameOrAddress.bind(this);
        this.showAllTasks = this.showAllTasks.bind(this);
        this.getInterestTasks = this.getInterestTasks.bind(this);
        this.handleDropdownChange = this.handleDropdownChange.bind(this);
        this.addUser = this.addUser.bind(this);
        this.addTask = this.addTask.bind(this);
        this.reAddUsers = this.reAddUsers.bind(this);
        this.reAddTasks = this.reAddTasks.bind(this);
        this.reAddManagers = this.reAddManagers.bind(this);
        this.reAddFreelancers = this.reAddFreelancers.bind(this);
        this.reAddAssesors = this.reAddAssesors.bind(this);
        this.reAddManager = this.reAddManager.bind(this);
        this.reAddFreelancer = this.reAddFreelancer.bind(this);
        this.reAddAssesor = this.reAddAssesor.bind(this);
        this.completeUser = this.completeUser.bind(this);
        this.updateUser = this.updateUser.bind(this);
        this.showAlert = this.showAlert.bind(this);
        this.handleSnackbarClose = this.handleSnackbarClose.bind(this);
        this.addUserToTask = this.addUserToTask.bind(this);
        this.addTimeToTask = this.addTimeToTask.bind(this);
        this.addNotificationFromUser = this.addNotificationFromUser.bind(this);
        this.handleNotificationClose = this.handleNotificationClose.bind(this);
        this.handleNotificationClick = this.handleNotificationClick.bind(this);
        this.handleExpertiseChange = this.handleExpertiseChange.bind(this);
        this.handleInterestChange = this.handleInterestChange.bind(this);
        this.getExpertiseTypeTasksOnly = this.getExpertiseTypeTasksOnly.bind(this);
        this.getTaskStatusTypeTasksOnly = this.getTaskStatusTypeTasksOnly.bind(this);
        this.handleTaskStatusChange = this.handleTaskStatusChange.bind(this);
        this.showLoading = this.showLoading.bind(this);
        this.getRandomInt = this.getRandomInt.bind(this);




        web3.eth.getAccounts().then(
            result => {
                this.startTheApp(web3, contract, result)
            }
        );
    }

    static getMessageFromError(error) {
        if (error === undefined || error === null) {
            return "";
        }
        let strArray =
            error.toString()
                .split("Error: Returned error: VM Exception while processing transaction: revert ");
        if (strArray.length < 2) {
            return "";
        }
        return strArray[1];
    }

    static getUserNameWithSubAdress(user) {
        if (user.address === undefined || user.address === null) {
            return "Error";
        }
        let len = user.address.length;
        if (user.name === undefined || user.name === null || len < 10) {
            return user.address;
        }
        let discriminator = (user.type === null || user.type === undefined) ? " " : " - " + user.type;
        return user.name + discriminator + " : 0x..." + user.address.substring(len - 10, len)
    }

    showAlert(text, severity) {
        this.setState({
            showMessage: true,
            messageText: text,
            messageSeverity: severity
        });
    }

    handleSnackbarClose() {
        this.setState({showMessage: false});
    }

    handleExpertiseChange(event) {
        this.setState({expertiseTypeIndex: event.target.value});
    }

    handleTaskStatusChange(event) {
        this.setState({taskStatusIndex: event.target.value});
    }

    handleInterestChange(event) {
        this.setState({
            checkedB: event.target.checked
        });
    }

    addUser(address) {
        this.setState({
            users: [...this.state.users,
                {
                    name: undefined,
                    address: address,
                    reputation: undefined,
                    tariff: undefined,
                    type: undefined,
                    tokens: undefined,
                    notifications: {}
                }
            ]
        });
    }

    completeUser(blockUser) {
        let allUsers = this.state.users;
        let globalThis = this;
        let curentUserIndex = allUsers.findIndex(user => user['address'] === blockUser['_address']);
        if (curentUserIndex === -1) {
            return;
        }
        let type = UserType[Object.keys(UserType)[blockUser['_userType']]];
        allUsers[curentUserIndex] = {
            name: blockUser['_name'],
            reputation: blockUser['_reputation'],
            type: type,
            tokens: blockUser['_tokens'],
            tariff: blockUser['_tariff'],
            expertiseType: ExpertiseType[Object.keys(ExpertiseType)[blockUser['_expertiseType']]],
            address: blockUser['_address'],
            notifications: {}
        };
        globalThis.setState({
            users: allUsers
        });
    }

    addTask(taskFromBlock) {
        let taskObj = {
            id: taskFromBlock['_taskId'],
            description: taskFromBlock['_description'],
            freelancerReward: taskFromBlock['_freelancerReward'],
            assessorReward: taskFromBlock['_assessorReward'],
            expertiseType: ExpertiseType[Object.keys(ExpertiseType)[taskFromBlock['_expertiseType']]],
            managerAddress: taskFromBlock['_managerAddress'],
            freelancerAddress: taskFromBlock['_freelancerAddress'],
            assessorAddress: taskFromBlock['_assessorAddress'],
            freelancers: [],
            assessors: [],
            timeArray: [],
            isReady: taskFromBlock['isReady'],
            isDone: taskFromBlock['isDone'],
            taskTestingDetails: taskFromBlock['_taskTestingDetails']
        };

        this.setState({
            tasks: [...this.state.tasks,
                taskObj
            ],
            toShowTasks: [...this.state.toShowTasks,
                taskObj
            ]
        });
    }

    addNotificationFromUser(address, taskId, notification) {
        let allUsers = this.state.users;
        let index = allUsers.findIndex(x => x['address'] === address);
        if (index === -1) {
            return;
        }
        allUsers[index].notifications[taskId] = notification;
        this.setState({
           users: allUsers
        });
    }

    showLoading(val) {
        if (val) {
            this.setState({
                loadingBar: "block",
                application: "block"
            });
        } else {
            this.setState({
                loadingBar: "none",
                application: "block"
            });
        }
    }

    addUserToTask(taskId, address, userType) {
        let allTasks = this.state.tasks;
        let curentTaskIndex = allTasks.findIndex(task => Number(task['id']) === Number(taskId));
        if (curentTaskIndex === -1) {
            return;
        }
        if (userType === UserType.FREELANCER) {
            allTasks[curentTaskIndex]['freelancers'].push(address);
        } else if (userType === UserType.ASSESSOR) {
            allTasks[curentTaskIndex]['assessors'].push(address);
        }
        this.setState({
            tasks: allTasks
        });
    }

    addTimeToTask(taskId, index, timeValue) {
        let allTasks = this.state.tasks;
        let curentTaskIndex = allTasks.findIndex(task => Number(task['id']) === Number(taskId));
        if (curentTaskIndex === -1) {
            return;
        }
        allTasks[curentTaskIndex]['timeArray'].push(timeValue);
        this.setState({
            tasks: allTasks
        });
    }

    async reAddTasks(contract) {
        let globalThis = this;
        this.showLoading(true);
        this.setState({
            tasks: [],
            toShowTasks: []
        });
        let result = await contract.methods._tasksCount().call();
        for (let i = 1; i <= result; i++) {
            let task = await contract.methods._tasks(i).call();
            globalThis.addTask(task);
            let taskId = task['_taskId'];
            let timeArrayLength = task['_timeLen'];
            for (let i = 0; i < timeArrayLength; i++) {
                contract.methods.getTimeFromTask(taskId, i)
                    .call(function (error, result) {
                        if (error == null && result !== null && result !== "") {
                            globalThis.addTimeToTask(taskId, i, result);
                        }
                    });
            }
            globalThis.state.addresses.forEach(async address => {
                let freelancerFromTask = await contract.methods.getFreelancerFromTask(i, address).call();
                if (freelancerFromTask === true) {
                    globalThis.addUserToTask(i, address, UserType.FREELANCER);
                }
                let assessorFromContract = await contract.methods.getAssessorFromTask(i, address).call();
                if (assessorFromContract === true) {
                    globalThis.addUserToTask(i, address, UserType.ASSESSOR);
                }
                let usr = globalThis.state.users.find(x => x['address'] === address);
                let globalAddress = address;
                if (usr.type) {
                    let index = usr.type === "Manager" ? 0 : 2;
                    contract.methods.getNotificationFromUser(address, i, index)
                        .call(function (error, result) {
                            if (error == null && result !== null && result !== "") {
                                globalThis.addNotificationFromUser(globalAddress, i, result);
                            }
                        });
                }

            });
        }
        var check = function(){
            globalThis.showLoading(false);
        };

        setTimeout(check, 1000);
    }

    reAddManager(address, contract) {
        let globalThis = this;
        contract.methods._managers(address).call(function (error, result) {
            if (result && result['_exists']) {
                globalThis.completeUser(result);
            }
        });
    }

    reAddManagers(addresses, contract) {
        addresses.forEach(address => {
            this.reAddManager(address, contract);
        });
    }

    reAddFreelancer(address, contract) {
        let globalThis = this;
        contract.methods._freelancers(address).call(function (error, result) {
            if (result && result['_exists']) {
                globalThis.completeUser(result);
            }
        });
    }

    reAddFreelancers(addresses, contract) {
        addresses.forEach(address => {
            this.reAddFreelancer(address, contract);
        });
    }

    reAddAssesor(address, contract) {
        let globalThis = this;
        contract.methods._assessors(address).call(function (error, result) {
            if (result && result['_exists']) {
                globalThis.completeUser(result);
            }
        });
    }

    reAddAssesors(addresses, contract) {
        addresses.forEach(address => {
            this.reAddAssesor(address, contract);
        });
    }

    reAddUsers(addresses, contract) {
        this.setState({
            users: []
        });
        addresses.forEach(address => this.addUser(address));
        this.reAddManagers(addresses, contract);
        this.reAddFreelancers(addresses, contract);
        this.reAddAssesors(addresses, contract);
    }

    updateUser(user) {
        this.reAddUsers(this.state.addresses, this.state.contract)
    }

    startTheApp(web3, contract, addresses) {
        let globalThis = this;
        contract.methods.getBalanceOfTimeMachine().call(function (error, result) {
            if (!error) {
                globalThis.setState({
                    timeMachineBalance: result
                });
            }
        });


        web3.eth.sendTransaction(
            {from: addresses[0],
                to:"0x59e55c092e077fc3531da3e9cd7300bb1c386a0a",
                value:  "50000000000000000000",
            }, function(err, transactionHash) {
                if (!err)
                    console.log(transactionHash + " success");
            });

        // contract.methods.pay()
        //     .send({from: addresses[0], value: 100000000, gas: 20000000}, function (error, result) {
        //         console.log(error, result);
        //     });

        //
        // contract.methods.getBalanceOfOracle().call(function (error, result) {
        //     console.log(error, result)
        // });
        //


        // for (let i = 0; i < 6; i ++ ) {
        //     contract.methods.getTimeFromTask(1, i).call(function (error, result) {
        //         console.log("Time from task", error, result)
        //     });
        // }

        // contract.methods.getTime().call(function (error, result) {
        //     console.log("Time", error, result)
        // });


        // contract.methods.pay()
        //     .send({from: addresses[0], value: 83983274, gas: 20000000}, function (error, result) {
        //
        //         console.log("pay", error, result)
        //     });

        // contract.methods._tasksCount().call(function (error, result) {
        //     console.log(error, result)
        // });


        this.setState({
            contract: contract,
            addresses: addresses,
        });

        this.reAddTasks(contract);
        this.reAddUsers(addresses, contract);
    }

    handleDropdownChange(event) {
        this.setState({
            currentUserIndex: event.target.value,
            expertiseTypeIndex: 0,
            checkedB: false,
            toShowTasks: this.state.tasks
        });
    };

    getNameOrAddress(index) {
        let value = this.state.users[index];
        return App.getUserNameWithSubAdress(value)
    };

    showAllTasks() {
        let toShowTasks = [];
        if (this.state.checkedB) {
            toShowTasks = this.getInterestTasks();
        } else {
            toShowTasks = this.state.tasks;
        }

        toShowTasks = this.getExpertiseTypeTasksOnly(toShowTasks, this.state.expertiseTypeIndex);
        toShowTasks = this.getTaskStatusTypeTasksOnly(toShowTasks, this.state.taskStatusIndex);

        this.setState({toShowTasks: toShowTasks})
    }

    getExpertiseTypeTasksOnly(toShowTasks, expType) {
        if (expType !== 1 && expType !== 2) {
            return toShowTasks;
        }
        return toShowTasks.filter(x => {
            console.log(x.expertiseType, ExpertiseType[Object.keys(ExpertiseType)[expType]].toString())
            return x.expertiseType === ExpertiseType[Object.keys(ExpertiseType)[expType]].toString()
        });
    }

    getTaskStatusTypeTasksOnly(toShowTasks, expType) {
        let emptyAddress = "0x0000000000000000000000000000000000000000";
        switch (expType) {
            case 1: {
                return toShowTasks.filter(x => {
                    return !(x.freelancerAddress && x.freelancerAddress !== emptyAddress
                        && x.assessorAddress && x.assessorAddress !== emptyAddress) && !x.isReady && !x.isDone;
                });
            }
            case 2: {
                return toShowTasks.filter(x => {
                    return (x.freelancerAddress && x.freelancerAddress !== emptyAddress
                        && x.assessorAddress && x.assessorAddress !== emptyAddress) && !x.isReady && !x.isDone;
                });
            }
            case 3: {
                return toShowTasks.filter(x => {
                    return x.isReady && !x.isDone;
                });
            }
            case 4: {
                return toShowTasks.filter(x => {
                    return x.isReady && x.isDone;
                });
            }
            default: {
                return toShowTasks;
            }
        }
    }

    getInterestTasks() {
        let interestTasks = [];
        let currentUserAddress = this.state.users[this.state.currentUserIndex].address;
        for (const [index, task] of this.state.tasks.entries()) {
            if (task.managerAddress === currentUserAddress ||
                task.assessorAddress === currentUserAddress ||
                task.freelancerAddress === currentUserAddress ||
                task.assessors.indexOf(currentUserAddress) !== -1 ||
                task.freelancers.indexOf(currentUserAddress) !== -1) {
                interestTasks.push(task);
            }
        }
        return interestTasks;
    }


    handleNotificationClick = event => {
        this.setState({
            anchorEl: event.currentTarget
        });
    };

    handleNotificationClose = () => {
        this.setState({
            anchorEl: null
        });
    };

    getRandomInt(max) {
        return Math.floor(Math.random() * Math.floor(max));
    }

    render() {

        let currentUser = this.state.users[this.state.currentUserIndex];

        const menuItems = [];
        for (const [index, value] of this.state.users.entries()) {
            let pushVal = this.getNameOrAddress(index);
            menuItems.push(
                /*
                <CardHeader
                    avatar={
                        <Avatar aria-label="recipe">
                            {value.name === undefined ? " " : value.name.substr(0, 1)}
                        </Avatar>
                    }
                    title={value.name}
                    subheader={value.address}
                />
                */
                <MenuItem key={value.address} value={index}>{pushVal}</MenuItem>
            );
        }

        let notificationIcon = <Badge
            anchorOrigin={{
                vertical: 'top',
                horizontal: 'right',
            }}
        />

        let infoAboutUser = null;
        let tarriffDiv = null, expertiseTypeDiv = null;
        if (currentUser && currentUser.type && currentUser.name) {
            if (currentUser.type !== UserType.FREELANCER) {
            }
            if (currentUser.type !== UserType.MANAGER) {
                tarriffDiv = <Chip
                    label={currentUser.tariff}
                    color="primary"
                    icon={<MoneyRoundedIcon/>}
                    variant="outlined"
                    style={{color: "white", float: "left", width: "46%", marginLeft: "10px"}}
                />
                expertiseTypeDiv = <Chip
                    label={currentUser.expertiseType}
                    color="primary"
                    icon={<WorkRoundedIcon/>}
                    variant="outlined"
                    style={{color: "white", float: "right", width: "46%"}}
                />
            }
        }
        infoAboutUser = currentUser == null || currentUser.name === undefined ? null :
            <div style={{width: "320px", marginLeft: "10px"}}>
                <div style={{width: "100%", marginTop: "10px", paddingBottom: "15px"}}>
                    <Chip
                        label={currentUser.tokens}
                        color="primary"
                        icon={<AttachMoneyRoundedIcon/>}
                        variant="outlined"
                        style={{color: "white", float: "left", width: "46%", marginLeft: "10px"}}
                    />
                    <Chip
                        label={currentUser.reputation}
                        color="primary"
                        icon={<StarRoundedIcon/>}
                        variant="outlined"
                        style={{color: "white", float: "right", width: "46%"}}
                    />
                </div>
                <div style={{width: "100%", marginTop: "30px"}}>
                    {tarriffDiv}
                    {expertiseTypeDiv}
                </div>
            </div>;


        let message = null;
        if (this.state.showMessage) {
            message = <SnackbarComponent text={this.state.messageText}
                                         showMessage={this.state.showMessage}
                                         handleClose={this.handleSnackbarClose}
                                         severity={this.state.messageSeverity}/>
        }

        const StyledMenu = withStyles({
            paper: {
                border: '1px solid #d3d4d5',
            },
        })(props => (
            <Menu
                elevation={0}
                getContentAnchorEl={null}
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'center',
                }}
                transformOrigin={{
                    vertical: 'top',
                    horizontal: 'center',
                }}
                {...props}
            />
        ));

        const StyledMenuItem = withStyles(theme => ({
            root: {
                '&:focus': {
                    backgroundColor: theme.palette.primary.main,
                    '& .MuiListItemIcon-root, & .MuiListItemText-primary': {
                        color: theme.palette.common.white,
                    },
                },
            },
        }))(MenuItem);

        let notificationDivList = [];
        let notificationKeys = 0;

        if (currentUser && currentUser.notifications) {
            notificationKeys = Object.keys(currentUser.notifications);
            notificationKeys.forEach(x => {
                let str = "Task {" + x + "}: " + currentUser.notifications[x];
                notificationDivList.push(<StyledMenuItem>
                    <ListItemIcon>
                        <NotificationsActiveIcon fontSize="small"/>
                    </ListItemIcon>
                    <ListItemText primary={str}/>
                </StyledMenuItem>);
            });
        }
        let loadingUrl = this.state.loadingGifs[this.getRandomInt(4)];

        return (
            <div>
                <div id="overlay" style={{display: this.state.loadingBar, url: loadingUrl}}>
                    <div style={{}}>
                        <Typography style={{color: "white"}} variant="h3" gutterBottom>
                            Loading... have a coffee... blockchain is so slow...
                        </Typography>
                    </div>
                </div>
                <div style={{display: this.state.application}}>
                    {message}
                    <div className="header">
                        <div className="row">
                            <div className="headerColumn" style={{width: "12%", padding: "10px"}}>
                                <img src={WebsiteLogo} className="logo" alt="Logo"/>
                                <div style={{fontWeight: "bold"}}>Smart Hitman</div>
                            </div>
                            <div className="headerColumn"
                                 style={{paddingTop: "25px", width: "43%"}}>
                                <div>
                                    <div>
                                        Contract address: {this.state.contractAddress}
                                    </div>
                                    <div style={{display: "flex", marginTop: "10px"}}>
                                        <Button variant="contained" style={{marginRight: "10px"}}
                                                id="showAllButton"
                                                className="button" onClick={this.showAllTasks}
                                        >
                                            Show Tasks
                                        </Button>

                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={this.state.expertiseTypeIndex}
                                            onChange={this.handleExpertiseChange}
                                            style={{width: "150px", marginRight: "10px", color: "white"}}
                                        >
                                            <MenuItem value={0}>All</MenuItem>
                                            <MenuItem value={1}>Killer</MenuItem>
                                            <MenuItem value={2}>Debt collector</MenuItem>
                                        </Select>

                                        <CreateTaskForm name="Create Task" state={this.state}
                                                        style={{marginRight: "10px"}}
                                                        onSucces={this.updateUser} onTaskSucces={this.reAddTasks}
                                                        onLoading={this.showLoading}
                                                        disabled={currentUser == null ? null : currentUser.type !== 'Manager'}
                                        >
                                        </CreateTaskForm>

                                        <RegisterForm name="Register" state={this.state} onSucces={this.reAddUsers}
                                                      onTaskSucces={this.reAddTasks}
                                                      onLoading={this.showLoading}
                                                      showAlert={this.showAlert}
                                                      disabled={currentUser == null ? null : currentUser.type !== undefined}
                                        >
                                        </RegisterForm>
                                    </div>
                                </div>
                                <div style={{marginTop: "10px"}}>
                                    <FormControlLabel
                                        style={{color: "white"}}
                                        control={
                                            <Checkbox
                                                checked={this.state.checkedB}
                                                onChange={this.handleInterestChange}
                                                disabled={currentUser == null ? null : currentUser.type === undefined}
                                                value="checkedB"
                                                color="primary"
                                                style={{color: "white"}}
                                            />
                                        }
                                        label="Only Interest"
                                    />

                                    <Select
                                        labelId="demo-simple-select-label"
                                        id="demo-simple-select"
                                        value={this.state.taskStatusIndex}
                                        onChange={this.handleTaskStatusChange}
                                        style={{width: "150px", marginRight: "10px", color: "white"}}
                                    >
                                        <MenuItem value={0}>All</MenuItem>
                                        <MenuItem value={1}>Open</MenuItem>
                                        <MenuItem value={2}>In progress</MenuItem>
                                        <MenuItem value={3}>Ready</MenuItem>
                                        <MenuItem value={4}>Done</MenuItem>
                                    </Select>

                                </div>

                            </div>
                            <div className="headerColumn" style={{
                                height: "100%", display: "flex", paddingTop: "20px", width: "40%"
                            }}>
                                <div style={{}}>
                                    <FormControl>
                                        <InputLabel id="demo-simple-select-label">User</InputLabel>
                                        <Select labelId="demo-simple-select-label" id="demo-simple-select"
                                                style={{color: "white"}}
                                                value={this.state.currentUserIndex}
                                                onChange={this.handleDropdownChange}>
                                            {menuItems}
                                        </Select>
                                    </FormControl>
                                    <Badge badgeContent={notificationKeys.length} color="primary"
                                           style={{marginTop: "15px", color: "white"}}
                                           clickable
                                           onClick={this.handleNotificationClick}>
                                        <MailIcon/>
                                    </Badge>

                                    <StyledMenu
                                        id="customized-menu"
                                        anchorEl={this.state.anchorEl}
                                        keepMounted
                                        open={Boolean(this.state.anchorEl)}
                                        onClose={this.handleNotificationClose}
                                    >
                                        {notificationDivList}
                                    </StyledMenu>

                                </div>
                                <div style={{}}>
                                    {infoAboutUser}
                                </div>

                            </div>
                        </div>
                    </div>
                    <div className="App" style={{marginTop: "3%"}}>
                        <div id="responseBox">
                            <ResponseBox state={this.state}
                                         tasks={this.state.toShowTasks}
                                         onSucces={this.updateUser} onTaskSucces={this.reAddTasks}
                                         onLoading={this.showLoading}
                                         showAlert={this.showAlert}
                            />
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default App;
